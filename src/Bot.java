/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import acciones.AutoDump;
import dataBase.ReguladorConexion;
import datos.Parada;
import datos.Persona;
import principal.BotTelegram;
import com.vdurmont.emoji.EmojiParser;
import dataBase.Soap;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.exceptions.TelegramApiException;
import utils.time.Timer;

/**
 *
 * @author ad_ri
 */
public class Bot {

    private static final long TIEMPO_REFRESCO_CONEXION_MILIS = 1 * 60 * 60 * 1000; //cada hora
    private static final long TIEMPO_GENERACION_COPIA_SEGURIDAD = 86400000L; //un día

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Iniciando bot...");
        String port, usu, pass;
        if (args.length == 3) {
            port = args[0];
            usu = args[1];
            pass = args[2];
        } else {
            System.out.print("Puerto: ");
            port = scan.nextLine();
            System.out.print("Usuario: ");
            usu = scan.nextLine();
            System.out.print("Contraseña: ");
            pass = scan.nextLine();
        }
        ReguladorConexion regcon = iniciarDB(usu, pass, port);
        BotTelegram bot = iniciarBot(regcon);
        initAutoDump(pass, usu, "127.0.0.1", bot).start();
        System.out.print(">");
        String linea = "";
        while ((linea = scan.nextLine()) != null) {
            switch (linea.toLowerCase()) {
                case "exit":
                    System.out.println("Saliendo...");
                    System.exit(0);
                    break;
                case "añadir paradas":
                    System.out.println("Ingrese la ruta del codigo fuente de la pagina a añadir:");
                    System.out.println("Escriba \"exit\" para salir.");
                    System.out.print("Ruta>");
                    while (true) {
                        String url = scan.nextLine();
                        if (url.toLowerCase().equals("exit")) {
                            break;
                        } else {
                            bot.añadirParadasHTML(url);
                            System.out.print("Ruta>");

                        }

                    }
                    break;
                case "añadir paradas url":
                    System.out.println("Ingrese la url de la pagina a añadir:");
                    System.out.println("Escriba \"exit\" para salir.");
                    System.out.print("URL>");
                    while (true) {
                        String url = scan.nextLine();
                        if (url.toLowerCase().equals("exit")) {
                            break;
                        } else {
                            if (bot.añadirParadasURL(url)) {
                                try {
                                    BufferedWriter bw = new BufferedWriter(new FileWriter("urls.txt", true));
                                    bw.write(url + "\n");
                                    bw.close();
                                } catch (IOException ex) {
                                    Logger.getLogger(Bot.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }
                            System.out.print("URL>");

                        }

                    }
                    break;
                case "autoupdate":
                    System.out.println("Introduzca la ruta del fichero de urls:");
                    String path = scan.next();
                    System.out.println("Va a comenzar la actualización automática de paradas (fuerza bruta)");
                    try {
                        BufferedReader br = new BufferedReader(new FileReader(path));
                        String url = null;
                        while ((url = br.readLine()) != null) {
                            bot.añadirParadasURL(url);
                        }
                        br.close();
                        System.out.println("Se ha terminado de actualizar.");
                    } catch (IOException ex) {
                        System.out.println("No se ha podido actualizar automáticamente");
                    }
                    break;
                case "refresh":
                    System.out.println(regcon.refresh());
                    break;
            }
            System.out.print(">");
        }
    }

    private static Timer initAutoDump(String pass, String user, String host, BotTelegram bot) {
        return bot.setAutodump(new AutoDump(pass, user, host, bot, TIEMPO_GENERACION_COPIA_SEGURIDAD));
    }

    /**
     * Inicia el bot
     *
     * @param conn La conexión con la base de datos
     */
    private static BotTelegram iniciarBot(ReguladorConexion conn) {
        ApiContextInitializer.init();
        TelegramBotsApi botsApi = new TelegramBotsApi();
        HashMap<Long, Persona> map = dataBase.Basics.cargarPersonas(conn.getConnection(), true);
        HashMap<Integer, Parada> paradas = dataBase.Basics.cargarParadas(conn.getConnection(), true);
        BotTelegram bot = null;
        try {
            bot = new BotTelegram(map, paradas, conn);
            botsApi.registerBot(bot);
            System.out.println("Bot iniciado" + EmojiParser.parseToUnicode(" :smile: :alien:"));
            bot.empezarHiloActualizacion();
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }

        return bot;
    }

    /**
     * Inicia la conexión con la base de datos bot
     *
     * @param conn La conexión con la base de datos
     */
    private static ReguladorConexion iniciarDB(String usuario, String psswd, String port) {
        System.out.println("Iniciando base de datos...");
        if (dataBase.Basics.crearBaseDatos(usuario, psswd, port)) {
            System.out.println("Conectando con la base de datos...");
            try {
                ReguladorConexion regcon = new ReguladorConexion(TIEMPO_REFRESCO_CONEXION_MILIS, usuario, psswd, port);
                System.out.println("Conexión realziada con éxito");
                return regcon;
            } catch (SQLException | ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
                System.err.println("No se ha podido conectar con la base de datos");
                System.err.println("Saliendo...");
                System.exit(0);
            }
        } else {
            System.err.println("No se ha podido conectar con la base de datos");
            System.err.println("Saliendo...");
            System.exit(0);
        }
        return null;
    }
}
