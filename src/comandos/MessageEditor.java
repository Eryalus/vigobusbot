/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comandos;

import java.util.ArrayList;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardRemove;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;

/**
 *
 * @author eryalus
 */
public class MessageEditor {

    
    public static SendMessage setTecladoSalir(SendMessage m) {
        ReplyKeyboardMarkup kb = new ReplyKeyboardMarkup();
        ArrayList<KeyboardRow> al = new ArrayList<>();
        KeyboardRow r = new KeyboardRow();
        r.add("Salir");
        al.add(r);
        kb.setKeyboard(al);
        m.setReplyMarkup(kb);
        return m;
    }
    
    public static SendMessage setTecladoTiempoBusqueda(SendMessage m) {
        ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();
        ArrayList<KeyboardRow> al = new ArrayList<>();
        KeyboardRow r = new KeyboardRow();
        r.add("3");
        al.add(r);
        r = new KeyboardRow();
        r.add("5");
        al.add(r);
        r = new KeyboardRow();
        r.add("10");
        al.add(r);
        r = new KeyboardRow();
        r.add("Salir");
        al.add(r);

        keyboardMarkup.setKeyboard(al);
        m.setReplyMarkup(keyboardMarkup);
        return m;
    }
    
    public static SendMessage setTeclado(String t, SendMessage m, boolean fav, Integer numero_botones, ArrayList<String> lista) {
        ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();
        ArrayList<KeyboardRow> al = new ArrayList<>();
        KeyboardRow r = new KeyboardRow();
        Integer tamaño = "Seguimientos: ".length() + "Salir.".length() + 1;
        if (!fav) {
            r.add("Añadir a favoritos");
            tamaño += "Añadir a favoritos".length();
            al.add(r);
            r = new KeyboardRow();
        }
        String[] lineas = t.split("\n");
        ArrayList<String> yaañadidas = new ArrayList<>();
        int cont = 0, contmax = 0;
        for (String lin : lineas) {
            if (utils.Basics.minutosPorLinea(lin) >= Comando.MINUTOS_MINIMOS && !yaañadidas.contains(lin)) {
                if (lista != null) {
                    lista.add(lin);
                }
                if (cont == 2) {
                    al.add(r);
                    cont = 0;
                    r = new KeyboardRow();
                }
                String[] p = lin.split(" ");
                if (p.length >= 2) {
                    r.add(lin.split("<b>")[0]);
                    tamaño += lin.split("<b>")[0].length();
                    if (tamaño > 4000) {
                        break;
                    }
                    yaañadidas.add(lin);
                    cont++;
                    contmax++;
                }
                if (contmax == numero_botones) {
                    break;
                }
            }
        }
        if (!r.isEmpty()) {
            al.add(r);
        }
        r = new KeyboardRow();
        r.add("Salir");
        al.add(r);
        keyboardMarkup.setKeyboard(al);
        if (keyboardMarkup.getKeyboard().isEmpty()) {
            m = removeTeclado(m);
        } else {
            m.setReplyMarkup(keyboardMarkup);
        }
        return m;
    }

    
    public static SendMessage removeTeclado(SendMessage m) {
        ReplyKeyboardRemove keyboardMarkup = new ReplyKeyboardRemove();
        m.setReplyMarkup(keyboardMarkup);
        return m;
    }
}
