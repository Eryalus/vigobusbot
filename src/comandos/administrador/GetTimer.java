/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comandos.administrador;

import principal.BotTelegram;
import comandos.Comando;
import java.util.ArrayList;
import org.telegram.telegrambots.api.methods.send.SendMessage;

/**
 *
 * @author eryalus
 */
public class GetTimer implements Comando {

    private BotTelegram bot;

    public GetTimer(BotTelegram bot) {
        this.bot = bot;
    }

    @Override
    public ArrayList<SendMessage> addMessages(ArrayList<SendMessage> ms) {
        SendMessage m = new SendMessage();
        Long time = bot.hiloactualizacion.getTimeInMillis();
        m.setText("<b>Tiempo de actualizacion:</b>\n\n" + time + "ms\n" + time / 1000 + "s\n" + time / 1000 / 60 + "m\n" + time / 1000 / 60 / 60 + "h");
        ms.add(m);
        return ms;
    }

}
