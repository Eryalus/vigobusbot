/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comandos.administrador;

import dataBase.ReguladorConexion;
import comandos.Comando;
import java.util.ArrayList;
import org.telegram.telegrambots.api.methods.send.SendMessage;

/**
 *
 * @author eryalus
 */
public class DeleteOP implements Comando {

    private String text;
    private ReguladorConexion conn;

    public DeleteOP(String text, ReguladorConexion conn) {
        this.text = text;
        this.conn = conn;
    }

    @Override
    public ArrayList<SendMessage> addMessages(ArrayList<SendMessage> ms) {
        String[] parts = text.split("\\s");
        SendMessage m = new SendMessage();
        if (parts.length == 2) {
            if (parts[1].startsWith("@")) {
                String user = parts[1].substring(1);
                Long id = dataBase.Basics.getIDbyUser(user, conn.getConnection());
                if (id >= 0) {
                    ArrayList<Long> admins = dataBase.Basics.cargarAdmins(conn, true);
                    if (!admins.contains(id)) {
                        m.setText("Ya no es administrador");
                    } else if (dataBase.Basics.delAdmin(id, conn.getConnection())) {
                        dataBase.Basics.delAdmin(id, conn.getConnection());
                        m.setText("Ahora " + user + " no es administrador");
                    } else {
                        m.setText("No se ha podido eliminar el administrador");
                    }

                } else {
                    m.setText("No se ha podido encotnrar al usuario en la base de datos");
                }
            } else {
                try {
                    ArrayList<Long> admins = dataBase.Basics.cargarAdmins(conn, true);
                    Long id = Long.parseLong(parts[1]);
                    if (!admins.contains(id)) {
                        m.setText("Ya no es administrador");
                    } else if (dataBase.Basics.delAdmin(id, conn.getConnection())) {
                        dataBase.Basics.delAdmin(id, conn.getConnection());
                        m.setText("Ahora " + id + " no es administrador");
                    } else {
                        m.setText("No se ha podido eliminar el administrador");
                    }
                } catch (NumberFormatException ex) {
                    m.setText("/delOP [numero de chat]|@[usuario]");
                }
            }
        } else {
            m.setText("/delOP [numero de chat]|@[usuario]");
        }
        ms.add(m);
        return ms;
    }

}
