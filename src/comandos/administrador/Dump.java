/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comandos.administrador;

import comandos.Comando;
import java.util.ArrayList;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import principal.BotTelegram;

/**
 *
 * @author eryalus
 */
public class Dump implements Comando {

    private final BotTelegram bot;
    private final String msg;
    private final Long id;

    public Dump(BotTelegram bot, String msg, Long chatID) {
        this.bot = bot;
        this.msg = msg;
        id = chatID;
    }

    @Override
    public ArrayList<SendMessage> addMessages(ArrayList<SendMessage> ms) {
        String[] parts = msg.split("\\s");
        boolean type = true;
        ArrayList<Long> al = null;
        switch (parts.length) {
            case 1:
                break;
            case 2:
                al = new ArrayList<>();
                al.add(id);
                type = false;
                break;
            default:
                ms.add(new SendMessage().setText("/dump own|all - Fuerza la creación de una copia de seguridad y la envia a los destinatarios."));
                return ms;
        }
        bot.getAutodump().forceDump(al, type);
        ms.add(new SendMessage().setText("Se ha enviado el forceDump."));
        return ms;
    }

}
