/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comandos.administrador;

import principal.BotTelegram;
import comandos.Comando;
import static comandos.MessageEditor.removeTeclado;
import datos.Persona;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.exceptions.TelegramApiException;

/**
 *
 * @author eryalus
 */
public class SendKeys implements Comando {

    private HashMap<Long, Persona> map;
    private Long chat_id;
    private String text;
    private BotTelegram bot;

    public SendKeys(HashMap<Long, Persona> map, Long chat_id, String text, BotTelegram bot) {
        this.map = map;
        this.chat_id = chat_id;
        this.text = text;
        this.bot = bot;
    }

    @Override
    public ArrayList<SendMessage> addMessages(ArrayList<SendMessage> ms) {
        try {
            SendMessage m = new SendMessage();
            m.setChatId(chat_id);
            m.setText("Se van a empezar a envíar los mensajes");
            bot.sendMessage(m);
            ArrayList<Long> al = new ArrayList<>();
            for (Long key : map.keySet()) {
                try {
                    m = new SendMessage();
                    m.setText(text);
                    m.setChatId(key);
                    bot.sendMessage(m);
                } catch (TelegramApiException ex) {
                    al.add(key);
                }
            }
            if (al.isEmpty()) {
                m.setText("Se han enviado todos los mensajes (" + map.size() + ") y ha vuelto al incio");
                m = removeTeclado(m);
                ms.add(m);
            } else {
                String temp = "No se han podido enviar los mensajes siguientes:\n\n";
                for (Long err : al) {
                    if ((temp.length() + ("" + err).length()) <= 4000) {
                        temp += err + "\n";
                    } else {
                        m = new SendMessage();
                        m.setText(temp);
                        ms.add(m);
                        temp = err + "\n";
                    }
                }
                m = new SendMessage();
                m.setText(temp + "\n\nY ha vuelto al inicio");
                m = removeTeclado(m);
                ms.add(m);

            }
            bot.putEstado(chat_id, 0);
        } catch (TelegramApiException ex) {
            SendMessage m = new SendMessage();
            m.setText("No se han podido enviar los mensajes, ha vuelto al inicio");
            m = removeTeclado(m);
            ms.add(m);
            bot.putEstado(chat_id, 0);
        }
        return ms;
    }

}
