/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comandos.administrador;

import principal.BotTelegram;
import comandos.Comando;
import static comandos.MessageEditor.setTecladoSalir;
import datos.Persona;
import estados.Estado;
import java.util.ArrayList;
import java.util.HashMap;
import org.telegram.telegrambots.api.methods.send.SendMessage;

/**
 *
 * @author eryalus
 */
public class SendTyped implements Comando {

    private Long chat_id;
    private String text;
    private BotTelegram bot;

    public SendTyped(Long chat_id, String text, BotTelegram bot) {
        this.chat_id = chat_id;
        this.text = text;
        this.bot = bot;
    }

    @Override
    public ArrayList<SendMessage> addMessages(ArrayList<SendMessage> ms) {
        SendMessage m = new SendMessage();
        String[] partes = text.split("\\s+");
        HashMap<Long, Persona> map = dataBase.Basics.cargarPersonas(bot.getConnection(), false);
        if (partes.length == 2) {
            switch (partes[1].toLowerCase()) {
                case "all":
                    bot.putToSend(chat_id, "all");
                    m.setText("Se enviará a todos los usuarios registrados (" + map.keySet().size() + ")\nEscriba el texto a enviar");
                    m = setTecladoSalir(m);
                    ms.add(m);
                    bot.putEstado(chat_id, Estado.ESTADO_EVNIAR_ADMIN);
                    break;
                case "seguimiento":
                    bot.putToSend(chat_id, "seguimiento");
                    m.setText("Se enviará a todos los usuarios que estén realizando algún seguimiento (" + bot.hiloactualizacion.getMap().keySet().size() + ")\nEscriba el texto a enviar");
                    m = setTecladoSalir(m);
                    ms.add(m);
                    bot.putEstado(chat_id, Estado.ESTADO_EVNIAR_ADMIN);
                    break;
                default:
                    if (partes[1].startsWith("@")) {
                        Long id = dataBase.Basics.getIDbyUser(partes[1].substring(1), bot.getConnection());
                        if (id == -1) {
                            m.setText("No se ha podido localizar a " + partes[1] + " en el registro");
                            ms.add(m);
                        } else {
                            bot.putToSend(chat_id, "" + id);
                            m.setText("Se enviará a \"" + dataBase.Basics.getPersona(id, bot.getConnection()) + "\"\nEscriba el texto a enviar");
                            m = setTecladoSalir(m);
                            ms.add(m);
                            bot.putEstado(chat_id, Estado.ESTADO_EVNIAR_ADMIN);
                        }
                    } else {
                        try {
                            Long nid = Long.parseLong(partes[1]);
                            Persona per = dataBase.Basics.getPersona(nid, bot.getConnection());
                            if (per != null) {
                                bot.putToSend(chat_id, partes[1]);
                                m.setText("Se enviará a \"" + per + "\"\nEscriba el texto a enviar");
                                m = setTecladoSalir(m);
                                ms.add(m);
                                bot.putEstado(chat_id, Estado.ESTADO_EVNIAR_ADMIN);
                            } else {
                                m.setText("No existe ningún usuario con ese chadID registrado");
                                ms.add(m);
                            }
                        } catch (NumberFormatException ex) {
                            m.setText("Uso: \"/send {all|seguimiento|@user|chatID}\"\nall - Todos\nseguimiento - Usuarios con algún seguimiento\n@user - usuario (cuidado con @null)\nchatID - al chat con ese ID");
                            ms.add(m);
                            break;
                        }
                    }
            }
        } else {
            m.setText("Uso: \"/send {all|seguimiento|@user|chatID}\"\nall - Todos\nseguimiento - Usuarios con algún seguimiento\n@user - usuario (cuidado con @null)\nchatID - al chat con ese ID");
            ms.add(m);
        }
        return ms;
    }

}
