/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comandos.administrador;

import comandos.Comando;
import comandos.usuario.Help;
import java.util.ArrayList;
import org.telegram.telegrambots.api.methods.send.SendMessage;

/**
 *
 * @author eryalus
 */
public class HelpOP implements Comando {

    @Override
    public ArrayList<SendMessage> addMessages(ArrayList<SendMessage> ms) {
        SendMessage m = new SendMessage();
        m.setText("<b>Usuarios</b>\n"
                + Help.HELP_TXT + "\n"
                + "\n<b>Administrador</b>\n"
                + "/addOP [numero de chat]|@[usuario] - Añadir un administrador\n"
                + "/delOP [numero de chat]|@[usuario] - Eliminar un administrador\n"
                + "/allseguimientos [usuario|parada|info] - Muestra todos los seguimientos\n"
                + "/send {all|seguimiento|@user|chatID} - Envia un mensaje\n"
                + "/log [total|error|status|all|last|clear] - Cambia el modo del logger\n"
                + "/settimer time [-ms|-s|-m|-h] - Cambiar el tiempo del timer\n"
                + "/gettimer - Informa del tiempo actual del timer\n"
                + "/getinfo id_chat - Informa de los datos del usuario de ese chat\n"
                + "/getIP - Informa de la IP de la máquina\n"
                + "/dump [own|all] - Fuerza la creación de una copia de seguridad y la envia a los destinatarios\n"
                + "/stop para el bot");
        ms.add(m);
        return ms;
    }

}
