/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comandos.administrador;

import datos.InfoActualizaciones;
import comprobaciones.HiloActualizacionParadas;
import comandos.Comando;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Set;
import org.telegram.telegrambots.api.methods.send.SendMessage;

/**
 *
 * @author eryalus
 */
public class GetSeguimientosOP implements Comando {

    private String text;
    private HiloActualizacionParadas hilo;

    public GetSeguimientosOP(String text, HiloActualizacionParadas hilo) {
        this.text = text;
        this.hilo = hilo;
    }

    @Override
    public ArrayList<SendMessage> addMessages(ArrayList<SendMessage> ms) {
        String[] parts = text.split("\\s+");
        Integer sort = null; //ordena pr parada
        switch (parts.length) {
            case 1:
                sort = 1;
                break;
            case 2:
                switch (parts[1].toLowerCase()) {
                    case "usuario":
                    case "user":
                    case "usu":
                    case "u":
                        sort = 0;
                        break;
                    case "parada":
                    case "par":
                    case "p":
                        sort = 1;
                        break;
                    case "info":
                    case "inf":
                    case "i":
                        sort = -1;
                        break;
                    default:
                        SendMessage m = new SendMessage();
                        m.setText("Uso: \"/allseguimientos [usuario|parada|info]\"\n-usuario: muestra por usuarios\n-parada:muestra por paradas\n-info:muestra estadisticas");
                        ms.add(m);
                        break;
                }
                break;
            default:
                SendMessage m = new SendMessage();
                m.setText("Uso: \"/allseguimientos [usuario|parada|info]\"\n-usuario: muestra por usuarios\n-parada:muestra por paradas\n-info:muestra estadisticas");
                ms.add(m);
                break;
        }
        if (sort != null) {
            HashMap<Long, ArrayList<InfoActualizaciones>> map = hilo.getMap();
            Set<Long> keys = map.keySet();
            ArrayList<InfoActualizaciones> al = new ArrayList<>();
            switch (sort) {
                case -1:
                    ArrayList<Integer> paradas = new ArrayList<>();
                    Integer seg = 0,
                     personas = keys.size();
                    for (Long key : keys) {
                        for (InfoActualizaciones array : map.get(key)) {
                            seg++;
                            if (!paradas.contains(array.getParada())) {
                                paradas.add(array.getParada());
                            }
                        }
                    }
                    SendMessage mt = new SendMessage();
                    mt.setText("<b>Informacion</b>\nTotal de paradas: " + paradas.size() + "\nTotal personas: " + personas + "\nTotal seguimientos: " + seg);
                    ms.add(mt);
                    break;
                case 0: {
                    String temp = "";
                    int contador = 0;
                    for (Long key : keys) {
                        temp += "<b>ChatID " + key + "</b>\n";
                        for (InfoActualizaciones array : map.get(key)) {
                            contador++;
                            if ((temp.length() + array.toString().length()) <= 3000) {
                                temp += array.toString() + "\n";
                            } else {
                                SendMessage m = new SendMessage();
                                m.setText(temp + array);
                                ms.add(m);
                                temp = "";
                            }
                        }
                    }
                    String tmp = "Total seguimientos: " + contador;
                    if ((temp.length() + tmp.length()) <= 3998) {
                        SendMessage m = new SendMessage();
                        m.setText(temp + "\n\n" + tmp);
                        ms.add(m);
                    } else {
                        SendMessage m = new SendMessage();
                        m.setText(temp);
                        ms.add(m);
                        m = new SendMessage();
                        m.setText(tmp);
                        ms.add(m);
                    }
                    break;
                }
                case 1: {
                    for (Long key : keys) {
                        for (InfoActualizaciones array : map.get(key)) {
                            al.add(array.setSort(sort).setID(key));
                        }
                    }
                    Collections.sort(al); //queda ordenado
                    String inicio = "", temp = "";
                    switch (sort) {
                        case 1:
                            inicio = "<b>Parada ";
                            Integer parada = -1;
                            for (InfoActualizaciones info : al) {
                                if (!info.getParada().equals(parada)) {
                                    parada = info.getParada();
                                    if ((temp.length() + (inicio + parada).length()) <= 4000) {
                                        temp += inicio + info.getParada() + "</b>\n";
                                    } else {
                                        SendMessage m = new SendMessage();
                                        m.setText(temp);
                                        ms.add(m);
                                        temp = "";
                                        temp += inicio + parada + "</b>\n";
                                    }
                                }
                                if ((temp.length() + ("ID: " + info.getID() + ", " + info + "\n").length()) <= 4000) {
                                    temp += "ID: " + info.getID() + ", " + info + "\n";
                                } else {
                                    SendMessage m = new SendMessage();
                                    m.setText(temp);
                                    ms.add(m);
                                    temp = "ID: " + info.getID() + ", " + info + "\n";
                                }

                            }
                            if (!temp.equals("")) {
                                SendMessage m = new SendMessage();
                                m.setText(temp + "\n\nTotal seguimientos: " + al.size());
                                ms.add(m);
                            }

                            break;
                        default:
                            break;
                    }
                    break;
                }
                default:
                    break;
            }
        }
        if (ms.isEmpty()) {
            SendMessage m = new SendMessage();
            m.setText("No hay seguimientos.");
            ms.add(m);
        }
        return ms;
    }

}
