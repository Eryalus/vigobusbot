/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comandos.administrador;

import principal.BotTelegram;
import principal.RespuestaConsulta;
import comandos.Comando;
import static comandos.MessageEditor.removeTeclado;
import datos.Persona;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.exceptions.TelegramApiException;

/**
 *
 * @author eryalus
 */
public class Send implements Comando {

    private Long chat_id;
    private String text;
    private BotTelegram bot;
    private RespuestaConsulta log;

    public Send(Long chat_id, String text, BotTelegram bot, RespuestaConsulta log) {
        this.chat_id = chat_id;
        this.text = text;
        this.bot = bot;
        this.log = log;
    }

    @Override
    public ArrayList<SendMessage> addMessages(ArrayList<SendMessage> ms) {
        Set<Long> keys;
        HashMap<Long, Persona> map;
        switch (bot.toSend(chat_id)) {
            case "all":
                map = dataBase.Basics.cargarPersonas(bot.getConnection(), false);
                ms = new SendKeys(map, chat_id, text, bot).addMessages(ms);
                break;
            case "seguimiento":
                map = dataBase.Basics.cargarPersonas(bot.getConnection(), false);
                keys = bot.hiloactualizacion.getMap().keySet();
                for (Long key : map.keySet()) {
                    if (!keys.contains(key)) {
                        map.remove(key);
                    }
                }
                ms = new SendKeys(map, chat_id, text, bot).addMessages(ms);
                break;
            default:
                Long id = new Long(-1);
                if (bot.toSend(chat_id).startsWith("@")) {
                    id = dataBase.Basics.getIDbyUser(bot.toSend(chat_id), bot.getConnection());
                } else {
                    try {
                        id = Long.parseLong(bot.toSend(chat_id));
                    } catch (NumberFormatException ex) {
                    }
                }
                if (id != -1) {
                    boolean error = false;
                    try {
                        SendMessage m = new SendMessage();
                        m.setText(text);
                        m.setChatId(id);
                        bot.sendMessage(m);
                    } catch (TelegramApiException ex) {
                        log.error(ex);
                        error = true;
                    }
                    SendMessage m = new SendMessage();
                    m = removeTeclado(m);
                    if (error) {
                        m.setText("No se ha podido enviar el mensaje a " + bot.toSend(chat_id));
                    } else {
                        m.setText("Se ha enviado correctamente a " + bot.toSend(chat_id));
                    }
                    ms.add(m);
                    bot.putEstado(chat_id, 0);
                } else {
                    SendMessage m = new SendMessage();
                    m.setText("Ha ocurrido un error");
                    m = removeTeclado(m);
                    bot.putEstado(chat_id, 0);
                    ms.add(m);
                }
                break;
        }
        return ms;
    }

}
