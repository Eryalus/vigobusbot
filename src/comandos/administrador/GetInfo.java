/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comandos.administrador;

import comandos.Comando;
import dataBase.Basics;
import dataBase.ReguladorConexion;
import datos.Persona;
import java.util.ArrayList;
import org.telegram.telegrambots.api.methods.send.SendMessage;

/**
 *
 * @author eryalus
 */
public class GetInfo implements Comando {

    private String text;
    private ReguladorConexion conn;

    public GetInfo(String text, ReguladorConexion conn) {
        this.text = text;
        this.conn = conn;
    }

    @Override
    public ArrayList<SendMessage> addMessages(ArrayList<SendMessage> ms) {
        SendMessage m = new SendMessage();
        String[] parts = text.split("\\s");
        if (parts.length == 2) {
            try {
                Long id = Long.parseLong(parts[1]);
                Persona p = Basics.cargarPersonas(conn.getConnection(),false).get(id);
                if (p != null) {
                    m.setText("Chat ID:" + id + "\nUser: @" + p.getUserName() + "\nNombre: " + p.getNombre() + " " + p.getApellidos());
                } else {
                    m.setText("Usuario no encontrado.");
                }
            } catch (NumberFormatException ex) {
                m.setText("Uso incorrecto.");
            }
        } else {
            m.setText("Uso incorrecto.");
        }
        ms.add(m);
        return ms;
    }

}
