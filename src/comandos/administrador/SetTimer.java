/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comandos.administrador;

import principal.BotTelegram;
import comandos.Comando;
import java.util.ArrayList;
import org.telegram.telegrambots.api.methods.send.SendMessage;

/**
 *
 * @author eryalus
 */
public class SetTimer implements Comando {

    private BotTelegram bot;
    private String txt;

    public SetTimer(BotTelegram bot, String txt) {
        this.bot = bot;
        this.txt = txt;
    }

    @Override
    public ArrayList<SendMessage> addMessages(ArrayList<SendMessage> ms) {
        SendMessage m = new SendMessage();
        String[] partes = txt.split("\\s+");
        try {
            if (partes.length == 1) {
                m.setText("Uso \"/settimer tiempo [-ms|-s|-m|-h]\" - Por defecto ms");
            } else {
                Integer t = Integer.parseInt(partes[1]);
                Float multiplicar = new Float(1);
                boolean ok = true;
                if (partes.length == 3) {
                    switch (partes[2]) {
                        case "-ms":
                            break;
                        case "-s":
                            multiplicar = new Float(1000);
                            break;
                        case "-m":
                            multiplicar = new Float(60000);
                            break;
                        case "-h":
                            multiplicar = new Float(3600000);
                            break;
                        default:
                            m.setText("Uso \"/settimer tiempo [-ms|-s|-m|-h]\" - Por defecto ms");
                            ok = false;
                            break;
                    }
                }
                if (partes.length <= 3 && ok) {
                    Long temp = (long) (t * multiplicar);
                    bot.hiloactualizacion.setTimeMillis(temp);
                    m.setText("<b>Tiempo  de actualizacion ajustado correctamente.</b>");
                } else {
                    m.setText("Uso \"/settimer tiempo [-ms|-s|-m|-h]\" - Por defecto s");
                }
            }
        } catch (Exception ex) {
            m.setText("Uso \"/settimer tiempo [-ms|-s|-m|-h]\" - Por defecto s");
        }

        ms.add(m);
        return ms;
    }

}
