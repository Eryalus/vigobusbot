/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comandos.administrador;

import principal.BotTelegram;
import principal.RespuestaConsulta;
import utils.Log;
import comandos.Comando;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import org.telegram.telegrambots.api.methods.send.SendDocument;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.exceptions.TelegramApiException;

/**
 *
 * @author eryalus
 */
public class LogActions implements Comando {

    private String text;
    private Long id;
    private BotTelegram bot;
    private RespuestaConsulta log;

    public LogActions(String text, Long id, BotTelegram bot, RespuestaConsulta log) {
        this.text = text;
        this.id = id;
        this.bot = bot;
        this.log = log;
    }

    @Override
    public ArrayList<SendMessage> addMessages(ArrayList<SendMessage> ms) {
        String[] parts = text.split("\\s+");
        SendMessage m = new SendMessage();
        boolean flag;
        if (parts.length == 2) {
            switch (parts[1].toLowerCase()) {
                case "total":
                    flag = bot.isFullLog();
                    if (flag) {
                        m.setText("Ya estaba en ese modo");
                    } else {
                        m.setText("Ahora el log registrará todas las interacciones");
                        bot.setFlagLog(true);
                    }
                    break;
                case "error":
                    flag = bot.isFullLog();
                    if (!flag) {
                        m.setText("Ya estaba en ese modo");
                    } else {
                        m.setText("Ahora el log solo registrará los errores");
                        bot.setFlagLog(false);
                    }
                    break;
                case "status":
                    if (bot.isFullLog()) {
                        m.setText("Está en modo total");
                    } else {
                        m.setText("Está en modo error");
                    }
                    break;
                case "clear":
                    File rt = new File(Log.getBaseRute());
                    File[] fs = rt.listFiles();
                    if (fs.length != 0) {
                        int contador = 0;
                        for (File f : fs) {
                            if (!f.getName().endsWith(".log")) {
                                f.delete();
                                contador++;
                            }
                        }
                        if (contador == 0) {
                            m.setText("Todos los ficheros eran logs, nada que borrar");
                        } else {
                            m.setText("Se han borrado " + contador + " archivos");
                        }
                    } else {
                        m.setText("No hay ningun fichero");
                    }
                    break;
                case "last":
                    try {
                        SendDocument doc = new SendDocument();
                        doc.setChatId(id);
                        File root = new File(Log.getBaseRute());
                        File[] files = root.listFiles();
                        if (files.length != 0) {
                            File tosend = null;
                            System.out.println(files);
                            for (int ind = 0; ind < files.length; ind++) {
                                if (files[ind].getName().endsWith(".log")) {
                                    tosend = files[ind];
                                    break;
                                }
                            }
                            if (tosend != null) {
                                doc.setNewDocument(tosend);
                                doc.setChatId(id);
                                bot.sendDocument(doc);
                                m.setText("Enviado correctamente");
                            } else {
                                m.setText("No hay ningun logger todavia");
                            }
                        } else {
                            m.setText("No hay ningun fichero todavia");
                        }
                    } catch (Exception ex) {
                        log.error(ex);
                        m.setText("No se ha podido enviar el fichero");
                    }
                    break;
                case "all":
                    try {
                        File fichero = Log.getAll();
                        if (fichero != null) {
                            SendDocument doc = new SendDocument();
                            doc.setNewDocument(fichero);
                            doc.setChatId(id);
                            bot.sendDocument(doc);
                            fichero.delete();
                            m.setText("Se han enviado todos los logs");
                        } else {
                            m.setText("No hay ningun logger todavia");
                        }
                    } catch (IOException | TelegramApiException ex) {
                        log.error(ex);
                        m.setText("No se ha podido enviar el fichero");
                    }
                    break;
                default:
                    m.setText("Usos: \"/log [total|error|status|all|last|clear]\"\n-total: hace log de todo\n-error: solo de los errores\n-status: informa del modo actual\n-all: envia todos los logs\n-last: envia el ultimo log\n-clear: borra lo que nos sean logs");
                    break;
            }
            ms.add(m);
        } else {
            m.setText("Usos: \"/log [total|error|status|all|last|clear]\"\n-total: hace log de todo\n-error: solo de los errores\n-status: informa del modo actual\n-all: envia todos los logs\n-last: envia el ultimo log\n-clear: borra lo que nos sean logs");
            ms.add(m);
        }
        return ms;
    }

}
