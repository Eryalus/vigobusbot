/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comandos.usuario;

import comandos.Comando;
import datos.Contacto;
import datos.ParadaFavorita;
import datos.Persona;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import org.telegram.telegrambots.api.methods.send.SendMessage;

/**
 *
 * @author eryalus
 */
public class MyInfo implements Comando {
    
    private final Persona p;
    private final ArrayList<Contacto> contactos;
    
    public MyInfo(Long chat_id, Connection con) {
        p = dataBase.Basics.getPersona(chat_id, con);
        contactos = dataBase.Basics.getContact(chat_id, con);
    }
    
    private boolean validString(String s) {
        if (s == null) {
            return false;
        }
        if (s.equals("null")) {
            return false;
        }
        return !s.isEmpty();
    }
    
    @Override
    public ArrayList<SendMessage> addMessages(ArrayList<SendMessage> ms) {
        SendMessage m = new SendMessage();
        if (p != null) {
            String txt = "ID=" + p.getID() + "\n";
            if (validString(p.getNombre())) {
                txt += "Nombre=" + p.getNombre() + "\n";
            }
            if (validString(p.getApellidos())) {
                txt += "Apellidos=" + p.getApellidos() + "\n";
            }
            if (validString(p.getUserName())) {
                txt += "Username=@" + p.getUserName() + "\n";
            }
            m.setText(txt);
            ms.add(m);
            HashMap<Integer, ParadaFavorita> favs = p.getFavs();
            if (favs != null) {
                txt = "Paradas favoritas:\n";
                for (ParadaFavorita parada : favs.values()) {
                    if ((txt.length() + (parada.getFavoritaI() + " - " + parada.getAlias()).length()) > 4000) {
                        SendMessage m2 = new SendMessage();
                        m2.setText(txt);
                        ms.add(m2);
                        if (validString(parada.getAlias())) {
                            txt = parada.getFavoritaI() + " - " + parada.getAlias() + "\n";
                        } else {
                            txt = parada.getFavoritaI() + "\n";
                        }
                    } else {
                        if (validString(parada.getAlias())) {
                            txt += parada.getFavoritaI() + " - " + parada.getAlias() + "\n";
                        } else {
                            txt += parada.getFavoritaI() + "\n";
                        }
                    }
                }
                if (!txt.equals("Paradas favoritas:\n")) {
                    SendMessage m2 = new SendMessage();
                    m2.setText(txt);
                    ms.add(m2);
                }
                for (Contacto c : contactos) {
                    SendMessage m3 = new SendMessage();
                    m3.setText("Contacto - " + c.getFormattedTime() + "\n" + c.getTxt());
                    ms.add(m3);
                }
            }
        } else {
            m.setText("No se han podido cargar los datos, inténtelo más tarde");
            ms.add(m);
        }
        return ms;
    }
    
}
