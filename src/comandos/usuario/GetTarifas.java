/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comandos.usuario;

import comandos.Comando;
import dataBase.Basics;
import java.io.IOException;
import java.util.ArrayList;
import org.telegram.telegrambots.api.methods.send.SendMessage;

/**
 *
 * @author eryalus
 */
public class GetTarifas implements Comando{

    @Override
    public ArrayList<SendMessage> addMessages(ArrayList<SendMessage> ms) {
        SendMessage m = new SendMessage();
        try {
            m.setText(Basics.readInfo("http://www.vitrasa.es/php/index.php?pag=tarifas/tarifas&tarifa=17"));
        } catch (IOException ex) {
           m.setText("No se han podido cargar las tarifas");
        }
        ms.add(m);
        return ms;
    }
    
}
