/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comandos.usuario;

import principal.RespuestaConsulta;
import comandos.Comando;
import static comandos.MessageEditor.removeTeclado;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import org.telegram.telegrambots.api.methods.send.SendMessage;

/**
 *
 * @author eryalus
 */
public class DeleteFav implements Comando {

    private final String texto;
    private final Long chat_id;
    private final Connection conn;
    private final RespuestaConsulta log;

    public DeleteFav(String texto, Long chat_id, Connection conn, RespuestaConsulta log) {
        this.texto = texto;
        this.chat_id = chat_id;
        this.conn = conn;
        this.log = log;
    }

    @Override
    public ArrayList<SendMessage> addMessages(ArrayList<SendMessage> ms) {
        SendMessage m = new SendMessage();
        if (texto.toLowerCase().equals("todas")) {
            try {
                Statement st = conn.createStatement();
                ArrayList<Integer> al = dataBase.Basics.getFavoritas(chat_id, st);
                String text = "";
                boolean error = false;
                for (Integer num : al) {
                    try {
                        dataBase.Basics.delFavorita(chat_id, num, st);
                    } catch (SQLException ex) {
                        error = true;
                        log.error(ex);
                        text += "No se ha podido borrar la parada " + num + " de favoritos\n";
                    }
                }
                if (!error) {
                    text += "Se han borrado todas las paradas de favoritos";
                } else {
                    text = text.substring(0, text.length() - 1);
                }
                m.setText(text);
            } catch (SQLException ex) {
                log.error(ex);
                m.setText("No se han podido borrar la parada de favoritos");
            }
        } else {
            try {
                Integer num = Integer.parseInt(texto);
                try {
                    Statement st = conn.createStatement();
                    dataBase.Basics.delFavorita(chat_id, num, st);
                    m.setText("Parada borrada de favoritos");
                } catch (SQLException ex) {
                    log.error(ex);
                    m.setText("No se ha podido borrar la parada de favoritos");
                }
            } catch (Exception ex) {
                m.setText("Introduzca el numero de parada a eliminar");
            }
        }
        m = removeTeclado(m);
        ms.add(m);
        return ms;
    }
}
