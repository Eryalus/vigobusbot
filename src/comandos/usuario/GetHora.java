/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comandos.usuario;

import dataBase.Soap;
import datos.Parada;
import datos.Persona;
import principal.RespuestaConsulta;
import comandos.Comando;
import comandos.MessageEditor;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import utils.Basics;

/**
 *
 * @author eryalus
 */
public class GetHora implements Comando {

    private String texto;
    private final Persona persona;
    private final HashMap<Integer, Parada> paradas;
    private final Connection conn;
    private final RespuestaConsulta log;

    /**
     *
     * @param texto
     * @param P
     * @param paradas
     * @param conn
     * @param log
     */
    public GetHora(String texto, Persona P, HashMap<Integer, Parada> paradas, Connection conn, RespuestaConsulta log) {
        this.texto = texto;
        this.persona = P;
        this.paradas = paradas;
        this.log = log;
        this.conn = conn;
    }

    private Integer numero = null;
    private ArrayList<String> buses_llegada = new ArrayList<>();

    public Integer getNumero() {
        return numero;
    }

    public ArrayList<String> getBuses() {
        return buses_llegada;
    }

    @Override
    public ArrayList<SendMessage> addMessages(ArrayList<SendMessage> ms) {
        ArrayList<Integer> al;
        SendMessage m = new SendMessage();
        if (texto.startsWith("/")) {
            texto = texto.substring(1);
        }
        try {
            numero = Integer.parseInt(texto);
            Parada p = paradas.get(numero);
            if (p != null) {
                try {
                    Statement st = conn.createStatement();
                    al = dataBase.Basics.getFavoritas(persona.getID(), st);
                    String t = Soap.getDatosParser(numero);//Utils.ConexionPaginas.descargarParada(p.getLatitud(), p.getLongitud(), p.getAltitud());

                    ms = Basics.getDataInMessages(t, ms, numero, p);
                    m = new SendMessage();
                    m.setText("Seguimientos:");
                    m = MessageEditor.setTeclado(t, m, al.contains(p.getNum()), MAXIMO_BOTONES, buses_llegada);
                    ms.add(m);
                } catch (IOException | InterruptedException ex) {
                    log.error(ex);
                    m.setText("Servidor del bus urbano no disponible");
                    ms.add(m);
                } catch (SQLException ex) {
                    log.error(ex);
                    m.setText("Error inesperado");
                    ms.add(m);
                }
            } else {
                m.setText("Ese número de parada no existe");
                ms.add(m);
            }
        } catch (Exception ex) {
            m.setText("Introduzca el número de parada por favor");
            ms.add(m);
        }
        return ms;
    }

}
