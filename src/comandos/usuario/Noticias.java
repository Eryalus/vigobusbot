/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comandos.usuario;

import comandos.Comando;
import datos.Noticia;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.telegram.telegrambots.api.methods.send.SendMessage;

/**
 *
 * @author eryalus
 */
public class Noticias implements Comando {

    @Override
    public ArrayList<SendMessage> addMessages(ArrayList<SendMessage> ms) {
        SendMessage m = new SendMessage();
        String txt = "";
        ArrayList<Noticia> noticias = readData();
        if (!noticias.isEmpty()) {
            for (Noticia n : noticias) {
                txt += n.getDate() + "\n<b>" + n.getTitle() + "</b>\n" + n.getDescription() + "\n" + n.getUrl() + "\n\n";
            }
            m.setText(txt);
        } else {
            m.setText("No se han podido cargar noticias");
        }
        ms.add(m);
        return ms;
    }

    public ArrayList<Noticia> readData() {
        ArrayList<Noticia> noticias = new ArrayList<>();
        try {
            int cont = 0;
            URL u = new URL(Noticia.URL);
            BufferedReader br = new BufferedReader(new InputStreamReader(u.openStream(), "ISO-8859-1"));
            String line;
            while ((line = br.readLine()) != null && cont < 5) {
                if (line.trim().startsWith("<span  class='text_pagina_actual_rojo' style='font-size:11px'>")) {
                    String date, title, url, desc;
                    if (line.split(">").length >= 2) {
                        date = line.split(">")[1].split("<")[0];
                        br.readLine();
                        line = br.readLine();
                        if (line.split("href='").length >= 2) {
                            url = line.split("href='")[1].split("'")[0];
                            if (line.split("<strong>").length >= 2) {
                                title = line.split("<strong>")[1].split("<")[0];
                                br.readLine();
                                br.readLine();
                                br.readLine();
                                line = br.readLine();
                                if (line.split("<p>").length >= 2) {
                                    desc = line.split("<p>")[1].split("<")[0];
                                    desc = HTMLParse(desc);
                                    Noticia n = new Noticia();
                                    n.setDate(date);
                                    n.setDescription(desc);
                                    n.setTitle(title);
                                    n.setUrl(url);
                                    noticias.add(n);
                                    cont++;
                                }

                            }
                        }
                    }
                }
            }
        } catch (MalformedURLException ex) {
        } catch (IOException ex) {
        }
        return noticias;
    }
    private static String[] ORIGINALS = new String[]{"&aacute;", "&eacute;", "&iacute;", "&oacute;", "&uacute;", "&Aacute;", "&Eacute;", "&Iacute;", "&Oacute;", "&Uacute;", "&euml;", "&nbsp;"};
    private static String[] DEST = new String[]{"á", "é", "í", "ó", "ú", "Á", "É", "Í", "Ó", "Ú", "ë", " "};

    private static String HTMLParse(String s) {
        for (int i = 0; i < ORIGINALS.length; i++) {
            s = s.replace(ORIGINALS[i], DEST[i]);
        }
        return s;
    }
}
