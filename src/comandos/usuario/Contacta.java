/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comandos.usuario;

import comandos.Comando;
import comandos.MessageEditor;
import java.util.ArrayList;
import org.telegram.telegrambots.api.methods.send.SendMessage;

/**
 *
 * @author eryalus
 */
public class Contacta implements Comando {

    @Override
    public ArrayList<SendMessage> addMessages(ArrayList<SendMessage> ms) {
        SendMessage m = new SendMessage();
        m.setText("<b>Contacta:</b>\n\nSi tienes cualquier duda, bug o sugerencia puedes enviarla aquí y se enviará a los responsables del bot");
        m = MessageEditor.setTecladoSalir(m);
        ms.add(m);
        return ms;
    }

}
