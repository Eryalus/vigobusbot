/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comandos.usuario;

import comandos.Comando;
import java.util.ArrayList;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import principal.BotTelegram;

/**
 *
 * @author eryalus
 */
public class DeleteEverything implements Comando {

    private final long chat_id;
    private final BotTelegram PARENT;

    public DeleteEverything(Long chat_id, BotTelegram PARENT) {
        this.chat_id = chat_id;
        this.PARENT = PARENT;
    }

    @Override
    public ArrayList<SendMessage> addMessages(ArrayList<SendMessage> ms) {
        SendMessage m = new SendMessage();
        if (dataBase.Basics.delPerson(chat_id, PARENT.getConnection())) {
            m.setText("Se han borrado todos los datos personales y las paradas favoritas.");
        } else {
            m.setText("No se han podido eliminar los datos. Inténtelo más tarde");
        }
        ms.add(m);
        return ms;
    }

}
