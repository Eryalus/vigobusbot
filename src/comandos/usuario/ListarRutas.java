/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comandos.usuario;

import comandos.Comando;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import lineas.Linea;
import lineas.Lineas;
import org.telegram.telegrambots.api.methods.send.SendMessage;

/**
 *
 * @author eryalus
 */
public class ListarRutas implements Comando {

    @Override
    public ArrayList<SendMessage> addMessages(ArrayList<SendMessage> ms) {
        SendMessage m = new SendMessage();
        try {
            ArrayList<Linea> lineas = Lineas.getAll();
            String text = "<b>Líneas:</b>\n";
            for (Linea lin : lineas) {
                text +=  "/"+lin.getIdentificador()+"R <b>"+lin.getName() +"</b>: "+ lin.getDesc() + "\n";
            }
            m.setText(text);
        } catch (IOException ex) {
            m.setText("No se han podido cargar las líneas. Disculpe las molestias.");
        }
        ms.add(m);
        return ms;
    }

}
