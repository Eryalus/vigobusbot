/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comandos.usuario;

import comandos.Comando;
import java.util.ArrayList;
import org.telegram.telegrambots.api.methods.send.SendMessage;

/**
 *
 * @author eryalus
 */
public class Help implements Comando {

    public static final String HELP_TXT = "/add - Añadir parada a favoritos\n"
            + "/del - Borrar parada(s) de favoritos\n"
            + "/fav - Mostrar las paradas favoritas\n"
            + "/seguimientos - Mostrar la lista de buses que sigue\n"
            + "/geo - (no necesario) Muestra las paradas mas cercanas\n"
            + "/buscar - Busqueda por texto\n"
            + "/tarifas - Muestra las tarifas actuales\n"
            + "/contacta - Informacion de contacto\n"
            + "/listarLineas - Muestra un listado de las líneas\n"
            + "/plano - Envia el plano de Vigo con las líneas de bus\n"
            + "/miInfo - Envia los datos almacenados\n"
            + "/borrarTodosLosDatos - Borra todos los datos que hay almacenados\n"
            + "/noticias - Muestra las ultimas 5 noticias\n"
            + "/donar - Información referente a donaciones\n"
            + "/calculoRuta - Calcula la mejor ruta entre dos puntos para llegar a la hora dada\n"
            + "/[numero linea]R - Muestra la ruta de una línea\n"
            + "/[numero parada]L - Muestra la localización de una línea\n"
            + "/[numero parada] - Buscar una parada";

    @Override
    public ArrayList<SendMessage> addMessages(ArrayList<SendMessage> ms) {
        SendMessage m = new SendMessage();
        m.setText(HELP_TXT);
        ms.add(m);
        return ms;
    }
}
