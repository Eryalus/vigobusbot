/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comandos.usuario;

import datos.Parada;
import datos.ParadaFavorita;
import principal.RespuestaConsulta;
import comandos.Comando;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;

/**
 *
 * @author eryalus
 */
public class AddFav implements Comando {

    private final int parada;
    private final String alias;
    private final Long chat_id;
    private final HashMap<Integer, Parada> paradas;
    private final Connection conn;
    private final RespuestaConsulta log;
    private final String texto;
    private final boolean type;
    private ParadaFavorita fav = null;

    //first
    public AddFav(Integer parada, String alias, Long chat_id, HashMap<Integer, Parada> paradas, Connection conn, RespuestaConsulta log) {
        this.parada = parada;
        this.alias = alias;
        this.chat_id = chat_id;
        this.paradas = paradas;
        this.conn = conn;
        this.log = log;
        texto = "";
        type = false;
    }

    //second
    public AddFav(String texto, Long chat_id, HashMap<Integer, Parada> paradas, Connection conn, RespuestaConsulta log) {
        this.log = log;
        this.paradas = paradas;
        this.conn = conn;
        this.chat_id = chat_id;
        this.texto = texto;
        this.alias = "";
        parada = 0;
        type = true;
    }

    @Override
    public ArrayList<SendMessage> addMessages(ArrayList<SendMessage> ms) {
        if (type) {
            return second(ms);
        } else {
            return first(ms);
        }
    }

    private ArrayList<SendMessage> first(ArrayList<SendMessage> ms) {
        SendMessage m = new SendMessage();
        if (!paradas.containsKey(parada)) {
            m.setText("Parada no encontrada");
            ms.add(m);
            return ms;
        }
        try {
            Statement st = conn.createStatement();
            dataBase.Basics.addFavorita(chat_id, new ParadaFavorita(parada, alias), st);
            m.setText("Parada añadida a favoritos");
        } catch (SQLException ex) {
            log.error(ex);
            m.setText("No se ha podido añadir la parada a favoritos");
        }
        ms.add(m);
        return ms;
    }

    private ArrayList<SendMessage> second(ArrayList<SendMessage> ms) {
        SendMessage m = new SendMessage();
        try {
            Integer numero = Integer.parseInt(texto.trim());
            Statement st = conn.createStatement();
            ArrayList<Integer> al = dataBase.Basics.getFavoritas(chat_id, st);
            if (!al.contains(numero)) {
                if (paradas.get(numero) != null) {
                    ParadaFavorita fav = new ParadaFavorita(numero, null);
                    dataBase.Basics.addFavorita(chat_id, fav, st);
                    m.setText("¿Desea ponerle un nombre personalizado a esta parada?");
                    ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();
                    ArrayList<KeyboardRow> alk = new ArrayList<>();
                    KeyboardRow r = new KeyboardRow();
                    r.add("Poner nombre");
                    alk.add(r);
                    r = new KeyboardRow();
                    r.add("Cancelar");
                    alk.add(r);
                    r = new KeyboardRow();
                    r.add("Salir");
                    alk.add(r);
                    keyboardMarkup.setKeyboard(alk);
                    m.setReplyMarkup(keyboardMarkup);
                    this.fav = fav;
                } else {
                    m.setText("Ese numero de parada no se corresponde con ninguna registrada");
                }
            } else {
                m.setText("Esa parada ya se encuentra en favoritos. Si desea ponerle un nombre borrela y vuelva a añadirla");
            }
        } catch (NumberFormatException ex) {
            m.setText("Escriba el numero de parada a añadir");
        } catch (SQLException ex) {
            log.error(ex);
            m.setText("No se ha podido añadir la parada a favoritos");
        }
        ms.add(m);
        return ms;
    }

    public ParadaFavorita getFavorita() {
        return fav;
    }
}
