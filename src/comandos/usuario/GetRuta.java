/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comandos.usuario;

import comandos.Comando;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import lineas.Linea;
import lineas.Lineas;
import org.telegram.telegrambots.api.methods.send.SendDocument;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.methods.send.SendPhoto;
import org.telegram.telegrambots.exceptions.TelegramApiException;
import principal.BotTelegram;

/**
 *
 * @author eryalus
 */
public class GetRuta implements Comando {

    private final long chat_id;
    private final String ident;
    private final BotTelegram BOT;

    public GetRuta(Long chatID, String ident, BotTelegram bot) {
        this.chat_id = chatID;
        BOT = bot;
        this.ident = ident;
    }

    @Override
    public ArrayList<SendMessage> addMessages(ArrayList<SendMessage> ms) {
        SendMessage m = new SendMessage();
        try {
            ArrayList<Linea> lineas = Lineas.getAll();
            boolean sended = false;
            for (Linea lin : lineas) {
                if (lin.getIdentificador().equals(ident)) {
                    sended = true;
                    SendPhoto sp = new SendPhoto();
                    sp.setChatId(chat_id);
                    sp.setCaption("Ruta de la línea " + lin.getName() + " " + lin.getDesc());
                    String[] media = Lineas.getRecorrido(lin);
                    sp.setPhoto(media[0]);
                    SendDocument sd = new SendDocument();
                    sd.setChatId(chat_id);
                    sd.setCaption("Horario detallado de la línea " + lin.getName() + " " + lin.getDesc());
                    
                    sd.setDocument(media[1]);
                    try {
                        BOT.sendPhoto(sp);
                        BOT.sendDocument(sd);
                        m.setText("Se ha enviado la ruta.");
                    } catch (TelegramApiException ex) {
                        ex.printStackTrace();
                        m.setText("Ha ocurrido un problema al enviar la ruta/horario. Inténtelo más tarde.");
                    }
                    break;
                }
            }
            if (!sended) {
                m.setText("No se ha encontrado ninguna línea con ese identificador.");
            }
        } catch (IOException ex) {
            m.setText("No se ha podido cargar la ruta de la línea indicada.");
        }
        ms.add(m);
        return ms;
    }

}
