/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comandos.usuario;

import datos.InfoParada;
import principal.BotTelegram;
import comandos.Comando;
import comandos.MessageEditor;
import java.util.ArrayList;
import java.util.Calendar;
import org.telegram.telegrambots.api.methods.send.SendMessage;

/**
 *
 * @author eryalus
 */
public class SeguimientoLinea implements Comando {

    private final Long chat_id;
    private final String linea;
    private final Integer tiempo;
    private final InfoParada info;
    private final BotTelegram bot;

    public SeguimientoLinea(Long chat_id, String linea, Integer tiempo, InfoParada info,  BotTelegram bot) {
        this.chat_id = chat_id;
        this.linea = linea;
        this.tiempo = tiempo;
        this.info = info;
        this.bot = bot;
    }

    @Override
    public ArrayList<SendMessage> addMessages(ArrayList<SendMessage> ms) {
        SendMessage m = new SendMessage();
        Integer n_parada = info.getNumeroI();
        String t = info.getText();
        if (info.getTime() > Calendar.getInstance().getTimeInMillis()) {
            ArrayList<String> ps = info.getLlegadas();
            for (String lin : ps) {
                if (lin.startsWith(linea)) {
                    Integer time = utils.Basics.minutosPorLinea(lin);
                    //System.out.println(time + "  " + tiempo);
                    if (time <= tiempo) {
                        m.setText("Tiempo de seguimiento demasiado elevado");
                    } else if (bot.hiloactualizacion.addParada(n_parada, linea, tiempo, chat_id)) {
                        m.setText("El bus \"" + linea + "\" en la parada " + n_parada + " ha empezado a seguirse\nSe avisará cuando le falten " + tiempo + "minutos");
                    } else {
                        m.setText("Ya se está haciendo ese seguimiento");
                    }
                    break;
                }
            }

        } else {
            m.setText("Ha pasado el tiempo máximo permitido\nVuelva a buscar la parada");
        }
        m = MessageEditor.removeTeclado(m);
        ms.add(m);
        return ms;
    }

}
