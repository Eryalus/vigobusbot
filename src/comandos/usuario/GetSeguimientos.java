/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comandos.usuario;

import datos.InfoActualizaciones;
import principal.BotTelegram;
import comandos.Comando;
import java.util.ArrayList;
import org.telegram.telegrambots.api.methods.send.SendMessage;

/**
 *
 * @author eryalus
 */
public class GetSeguimientos implements Comando {

    private final Long chat_id;
    private final BotTelegram bot;

    public GetSeguimientos(Long chat_id, BotTelegram bot) {
        this.chat_id = chat_id;
        this.bot = bot;
    }

    @Override
    public ArrayList<SendMessage> addMessages(ArrayList<SendMessage> ms) {
        ArrayList<InfoActualizaciones> al = bot.hiloactualizacion.getActualizaciones(chat_id);
        String temp = "Estos son tus seguimientos:\n";
        for (InfoActualizaciones info : al) {
            String tempo = info.getLinea() + " en " + info.getParada() + " para " + info.getTiempo() + " minutos. Faltan <b>" + info.getRestante() + "</b> minutos\n";
            if ((temp.length() + tempo.length()) <= 4095) {
                temp += tempo;
            } else {
                SendMessage m = new SendMessage();
                m.setText(temp);
                ms.add(m);
                temp = tempo;
            }
        }
        if (temp.equals("Estos son tus seguimientos:\n")) {
            SendMessage m = new SendMessage();
            m.setText("No estás realizando ningun seguimiento");
            ms.add(m);
        } else {
            SendMessage m = new SendMessage();
            m.setText(temp);
            ms.add(m);
        }
        return ms;
    }
}
