/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comandos.usuario;

import com.vdurmont.emoji.EmojiParser;
import comandos.Comando;
import java.util.ArrayList;
import org.telegram.telegrambots.api.methods.send.SendMessage;

/**
 *
 * @author eryalus
 */
public class Start implements Comando {

    @Override
    public ArrayList<SendMessage> addMessages(ArrayList<SendMessage> ms) {
        SendMessage m = new SendMessage();
        m.setText(EmojiParser.parseToUnicode("Bienvenido al bot del bus urbano de Vigo :smile:"));
        ms.add(m);
        return ms;
    }
}
