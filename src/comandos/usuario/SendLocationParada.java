/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comandos.usuario;

import datos.Parada;
import principal.BotTelegram;
import comandos.Comando;
import static comandos.MessageEditor.removeTeclado;
import java.util.ArrayList;
import org.telegram.telegrambots.api.methods.send.SendLocation;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.exceptions.TelegramApiException;

/**
 *
 * @author eryalus
 */
public class SendLocationParada implements Comando {

    private final Long ID;
    private final String numero;
    private final BotTelegram bot;

    public SendLocationParada(Long ID, String numero, BotTelegram bot) {
        this.ID = ID;
        this.numero = numero;
        this.bot = bot;
    }

    @Override
    public ArrayList<SendMessage> addMessages(ArrayList<SendMessage> ms) {
        SendMessage m = new SendMessage();
        m = removeTeclado(m);
        try {
            Integer num = Integer.parseInt(numero);
            Parada p = dataBase.Basics.cargarParadas(bot.getConnection(), false).get(num);
            if (p != null) { //la contiene
                SendLocation l = new SendLocation();
                l.setLatitude((float) p.getLatitud());
                l.setLongitude((float) p.getLongitud());
                l.setChatId(ID);
                bot.sendLocation(l);
                m = null;
                //m.setText("Ubicacion de la parada:\n" + p);
            } else {
                m.setText("No hay parada con el numero " + num);
            }
        } catch (NumberFormatException ex) {
            m.setText("En texto introducido no es un numero");
        } catch (TelegramApiException ex) {
            m.setText("No se ha podido enviar la ubicacion de la parada");
        }
        if (m != null) {
            ms.add(m);
        }
        return ms;
    }

}
