/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comandos.usuario;

import datos.Parada;
import datos.ParadaFavorita;
import principal.RespuestaConsulta;
import comandos.Comando;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import org.telegram.telegrambots.api.methods.send.SendMessage;

/**
 *
 * @author eryalus
 */
public class GetFav implements Comando {

    private final Long chat_id;
    private final Connection conn;
    private final RespuestaConsulta log;
    private final HashMap<Integer, Parada> paradas;

    public GetFav(Long chat_id, HashMap<Integer, Parada> paradas, Connection conn, RespuestaConsulta log) {
        this.chat_id = chat_id;
        this.conn = conn;
        this.paradas = paradas;
        this.log = log;
    }

    @Override
    public ArrayList<SendMessage> addMessages(ArrayList<SendMessage> ms) {
        try {
            Statement st = conn.createStatement();
            ArrayList<ParadaFavorita> al = dataBase.Basics.getParadasFavoritas(chat_id, st);
            SendMessage m = new SendMessage();
            if (al.isEmpty()) {
                m.setText("No tienes paradas favoritas");
                ms.add(m);
            } else {
                ArrayList<String> parads = new ArrayList<>();
                for (ParadaFavorita fav : al) {
                    Parada par = paradas.get(fav.getFavoritaI());
                    if (par != null) {
                        if (!fav.getAlias().equals("null")) {
                            parads.add(par.getParWithSlash() + " \"" + fav.getAlias() + "\"\n");
                        } else {
                            parads.add(par.getParWithSlash() + "\n");
                        }
                    }
                }
                if (parads.isEmpty()) {
                    m.setText("No tienes paradas favoritas");
                    ms.add(m);
                } else {
                    String aux = "";
                    for (String pard : parads) {
                        if ((aux.length() + pard.length()) <= 4095) {
                            aux += pard + "\n";
                        } else {
                            SendMessage maux = new SendMessage();
                            maux.setText(aux);
                            ms.add(maux);
                            aux = "";
                        }
                    }
                    if (!aux.equals("")) {
                        SendMessage maux = new SendMessage();
                        maux.setText(aux);
                        ms.add(maux);
                    }
                }
            }
        } catch (SQLException ex) {
            log.error(ex);
            SendMessage m = new SendMessage();
            m.setText("Error cargando las paradas favoritas");
            ms.add(m);
        }
        return ms;
    }
}
