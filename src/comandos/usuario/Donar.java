/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comandos.usuario;

import comandos.Comando;
import java.util.ArrayList;
import org.telegram.telegrambots.api.methods.send.SendMessage;

/**
 *
 * @author eryalus
 */
public class Donar implements Comando {

    private static final String TEXT = "Si quieres hacer una donación para mantener y mejorar e bot puedes hacerlo en el siguiente link:\nhttps://paypal.me/Eryalus";

    @Override
    public ArrayList<SendMessage> addMessages(ArrayList<SendMessage> ms) {
        SendMessage m = new SendMessage();
        m.setText(TEXT);
        ms.add(m);
        return ms;
    }

}
