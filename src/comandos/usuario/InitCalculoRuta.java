/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comandos.usuario;

import comandos.Comando;
import comandos.MessageEditor;
import java.util.ArrayList;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;

/**
 *
 * @author eryalus
 */
public class InitCalculoRuta implements Comando {

    private static final String TEXT = "Introduzca la hora de salida en el formato HH:MM DD/MM/AAAA";

    @Override
    public ArrayList<SendMessage> addMessages(ArrayList<SendMessage> ms) {
        SendMessage m = new SendMessage();
        m.setText(TEXT);
        ReplyKeyboardMarkup kb = new ReplyKeyboardMarkup();
        ArrayList<KeyboardRow> al = new ArrayList<>();
        KeyboardRow r = new KeyboardRow();
        r.add("Ahora");
        al.add(r);
        r=new KeyboardRow();
        r.add("Salir");
        al.add(r);
        kb.setKeyboard(al);
        m.setReplyMarkup(kb);
        ms.add(m);
        return ms;
    }

}
