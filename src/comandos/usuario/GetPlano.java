/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comandos.usuario;

import comandos.Comando;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.telegram.telegrambots.api.methods.send.SendDocument;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.exceptions.TelegramApiException;
import principal.BotTelegram;

/**
 *
 * @author ad_ri
 */
public class GetPlano implements Comando {

    private final long chat_id;
    private final BotTelegram BOT;
    private final static String CAPTION = "Plano marquesinas 2016",
            URL_PLANO = "http://www.vitrasa.com/FotosFichas/descargas/PlanoMarquesina2016.pdf";

    public GetPlano(Long chatID, BotTelegram bot) {
        this.chat_id = chatID;
        BOT = bot;
    }

    @Override
    public ArrayList<SendMessage> addMessages(ArrayList<SendMessage> ms) {
        SendMessage m = new SendMessage();
        SendDocument sd = new SendDocument();
        sd.setChatId(chat_id);
        sd.setCaption(CAPTION);
        sd.setDocument(URL_PLANO);
        try {
            BOT.sendDocument(sd);
            m.setText("Se ha enviado el plano correctamente.");
        } catch (TelegramApiException ex) {
            ex.printStackTrace();
            m.setText("No se ha podido enviar el plano.");
        }
        ms.add(m);
        return ms;
    }

}
