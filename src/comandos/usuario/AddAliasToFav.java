/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comandos.usuario;

import datos.ParadaFavorita;
import principal.RespuestaConsulta;
import comandos.Comando;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import org.telegram.telegrambots.api.methods.send.SendMessage;

/**
 *
 * @author eryalus
 */
public class AddAliasToFav implements Comando {

    private final String texto;
    private final Long chat_id;
    private final ParadaFavorita fav;
    private final Connection conn;
    private final RespuestaConsulta log;
    private boolean ok = false;

    public AddAliasToFav(String texto, Long chat_id, ParadaFavorita fav, Connection conn, RespuestaConsulta log) {
        this.texto = texto;
        this.chat_id = chat_id;
        this.fav = fav;
        this.conn = conn;
        this.log = log;
    }

    @Override
    public ArrayList<SendMessage> addMessages(ArrayList<SendMessage> ms) {
        SendMessage m = new SendMessage();
        try {
            Statement st = conn.createStatement();
            dataBase.Basics.delFavorita(chat_id, fav.getFavoritaI(), st);
            ParadaFavorita pfav = new ParadaFavorita(fav.getFavoritaI(), texto.trim());
            dataBase.Basics.addFavorita(chat_id, pfav, st);
            m.setText("Nombre puesto con exito\n\nEscriba el numero de parada a añadir");
            ok = true;
        } catch (Exception ex) {
            log.error(ex);
            m.setText("No se ha podido poner nombre a la parada");
        }
        ms.add(m);
        return ms;
    }

    public boolean isOk() {
        return ok;
    }

}
