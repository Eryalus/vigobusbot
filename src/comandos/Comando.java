/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comandos;

import principal.RespuestaConsulta;
import java.util.ArrayList;
import org.telegram.telegrambots.api.methods.send.SendMessage;

/**
 *
 * @author eryalus
 */
public interface Comando {

    static final Integer MINUTOS_MINIMOS = 5,
            MAXIMO_BOTONES = 10; //a ser posible par para cuadrar botones

    /**
     *
     * @param ms
     * @return
     */
    public abstract ArrayList<SendMessage> addMessages(ArrayList<SendMessage> ms);
}
