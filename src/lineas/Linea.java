package lineas;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author eryalus
 */
public class Linea implements Comparable<Linea> {

    private String Name, Desc, code, Identificador;

    public String getIdentificador() {
        return Identificador;
    }

    public void setIdentificador(String Identificador) {
        this.Identificador = Identificador;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
        String[] ps = code.split("-");
        if (ps.length == 2) {
            this.Identificador = ps[1].split("_")[0];
        }
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getDesc() {
        return Desc;
    }

    public void setDesc(String Desc) {
        this.Desc = Desc;
    }

    @Override
    public int compareTo(Linea o) {
        return Name.compareTo(o.Name);
    }
}
