package lineas;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author eryalus
 */
public class Lineas {

    private static final String BASE_URL = "http://www.vitrasa.es/";
    private static final String[] TO_BE_REPLACED = {"Espa�a", "ESPA�A", "Urz�iz", "Am�rica", "AM�RICA", "ZAM�NS", "Barb�n", "MATAM�", "�LVARO",
        "V�A", " � ", "ESTACI�N", "COL�N", "S�RDOMA", "ARAG�N", "GARC�A", "BARB�N", "ENCARNACI�N", "SAI�NS", "MU��O", "GU�A", "R�O", "ANDR�S", " �",
        "SABAX�NS", "�"};
    private static final String[] NEW_TEXT = {"España", "ESPAÑA", "Urzáiz", "América", "AMÉRICA", "ZAMÁNS", "Barbón", "MATAMÁ", "ÁLVARO",
        "VÍA", " - ", "ESTACIÓN", "COLÓN", "SÁRDOMA", "ARAGÓN", "GARCÍA", "BARBÓN", "ENCARNACIÓN", "SAIÁNS", "MUÍÑO", "GUÍA", "RÍO", "ANDRÉS", " - ",
        "SABAXÁNS", ""};

    public static String[] getRecorrido(Linea lin) throws MalformedURLException, IOException {
        URL url = new URL(BASE_URL + lin.getCode());
        BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
        String[] ret = new String[2];
        String tmp, url_img = null, url_pdf = null;
        while ((tmp = br.readLine()) != null) {
            if (tmp.contains("termometro")) {
                //<img class='termometro' src='/FotosFichas/lineas14/C1-Termometro.png'>
                String[] trozos = tmp.split("'");
                if (trozos.length == 5) {
                    url_img = BASE_URL + trozos[3];
                }
            } else if (tmp.contains("<ul><li><a href=")) {
                String[] trozos = tmp.split("<ul><li><a href='");
                if (trozos.length == 2) {
                    url_pdf = BASE_URL +trozos[1].split("'")[0];
                }
            }
        }
        ret[0] = url_img;
        ret[1] = url_pdf;
        return ret;
    }

    public static ArrayList<Linea> getAll() throws MalformedURLException, IOException {
        ArrayList<Linea> al = new ArrayList<>();
        URL url = new URL(BASE_URL + "renovaciones");
        BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
        HashMap<String, Integer> map = new HashMap<>();
        HashMap<String, Linea> map_lineas = new HashMap<>();
        String tmp;
        while ((tmp = br.readLine()) != null) {
            if (tmp.contains("/renovaciones-") && tmp.startsWith("<tr")) {
                String[] parts = tmp.split("href='");
                for (String s : parts) {
                    String tmp2 = s.split("</a")[0];
                    String ur = tmp2.split("'")[0].replace("/", "");
                    if (map.containsKey(ur)) {
                        map.put(ur, map.get(ur) + 1);
                    } else {
                        map.put(ur, 1);
                    }
                    String desc = tmp2.split(">")[1];
                    if (map.get(ur) == 1) {
                        Linea lin = new Linea();
                        lin.setName(desc);
                        lin.setCode(ur);
                        map_lineas.put(ur, lin);
                    } else if (map.get(ur) == 2) {
                        Linea lin = map_lineas.get(ur);
                        for (int i = 0; i < TO_BE_REPLACED.length; i++) {
                            desc = desc.replace(TO_BE_REPLACED[i], NEW_TEXT[i]);
                        }
                        lin.setDesc(desc);

                    }
                }
                break;
            }

        }
        for (String s : map_lineas.keySet()) {
            if (!s.startsWith("<")) {
                al.add(map_lineas.get(s));
            }
        }
        Collections.sort(al);
        return al;
    }
}
