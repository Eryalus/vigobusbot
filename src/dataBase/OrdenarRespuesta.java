/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataBase;

/**
 *
 * @author eryalus
 */
public class OrdenarRespuesta implements Comparable {

    private final String Linea, nombre;
    private final Integer tiempo;

    public OrdenarRespuesta(String Linea, String nombre, String tiempo) {
        this.Linea = Linea;
        this.nombre = nombre;
        this.tiempo = Integer.parseInt(tiempo);
    }

    @Override
    public String toString() {
        return Linea + ": " + nombre + " <b>" + tiempo + "</b>minutos";
    }

    public Integer getTiempo() {
        return tiempo;
    }

    @Override
    public int compareTo(Object o) {
        if (((OrdenarRespuesta) o).getTiempo() <= tiempo) {
            return 1;
        } else if (((OrdenarRespuesta) o).getTiempo() >= tiempo) {
            return -1;
        } else {
            return 0;
        }
    }

}
