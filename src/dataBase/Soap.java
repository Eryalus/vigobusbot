/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataBase;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang.StringEscapeUtils;

/**
 *
 * @author eryalus
 */
public class Soap {

    private static final int SEGUNDOS_TIMEOUT = 5;
    private static final int MAXIMOS_BUSES = 50;

    private static final String P1 = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
            + "<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">\n"
            + "  <soap12:Body>\n"
            + "    <EstimacionParadaCoordenadas xmlns=\"http://tempuri.org/\">\n"
            + "      <LatitudParada>";
    private static final String P2 = "</LatitudParada>\n"
            + "      <LongitudParada>";
    private static final String P3 = "</LongitudParada>\n"
            + "    </EstimacionParadaCoordenadas>\n"
            + "  </soap12:Body>\n"
            + "</soap12:Envelope>";
    private static final String P4 = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
            + "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n"
            + "  <soap:Body>\n"
            + "    <EstimacionParadaIdParada xmlns=\"http://tempuri.org/\">\n"
            + "      <IdParada>";
    private static final String P5 = "</IdParada>\n"
            + "    </EstimacionParadaIdParada>\n"
            + "  </soap:Body>\n"
            + "</soap:Envelope>";

    private static String getInfo(String text) throws UnsupportedEncodingException {
        System.out.println(text);
        String resultado = "";
        String[] texts = text.replace("\n", "").split("&lt;Estimaciones&gt;");
        ArrayList<OrdenarRespuesta> al = new ArrayList<>();
        if (texts.length == 1) {
            return "No hay datos de llegadas";
        } else {
            int cont = 0;
            for (int i = 1; i < texts.length; i++) {
                if (cont < MAXIMOS_BUSES) {
                    String[] aux = texts[i].split(";");
                    String Linea = aux[2].split("&lt")[0];
                    String nombre = aux[6].split("&lt")[0];
                    String tiempo = aux[10].split("&lt")[0];
                    al.add(new OrdenarRespuesta(Linea, nombre, tiempo));
                    cont++;
                } else {
                    break;
                }
            }
            Collections.sort(al);
            for (OrdenarRespuesta or : al) {
                resultado += or + "\n";
            }
        }
        resultado = resultado.replace("Ã‰", "É").replace("Ã“", "Ó").replace("Ã‘", "Ñ").replace("Ã", "Í").replace("Ã", "Á");
        //System.out.println(resultado);
        return resultado;
    }

    private static final String[] ORIGINALS = {"Ã‰", "Ã“", "Ã‘", "Ã", "Ã"};
    private static final String[] FINAL = {"É", "Ó", "Ñ", "Í", "Á"};

    private static String changeChars(String text) {
        return text;
    }

    private static String peticion(String send) throws IOException {
        //crea un proceso nativo que se encarga de hacer la consulta
        String url = "http://sira.intecoingenieria.com/SWEstimacionParada.asmx"; //endpoTimeUnit.DAYSint
        String result = "No hay respuesta del servidor";
        String username = "user_name";
        String password = "pass_word";
        String[] command = {"curl", "-u", username + ":" + password, "-X", "POST", "-H", "Content-Type: text/xml", "-d", send, url};
        ProcessBuilder process = new ProcessBuilder(command);
        Process p;
        p = process.start();
        //hay que comprobar si no se fue a la kk en un tiempo
        try {
            //java 8
            if (p.waitFor(SEGUNDOS_TIMEOUT, TimeUnit.SECONDS)) {
                //ha terminado sin timeout
                BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
                StringBuilder builder = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    builder.append(line);
                    builder.append(System.getProperty("line.separator"));
                }
                result = builder.toString();
                return getInfo(result);
            } else {
                //timeout
                return result;
            }
        } catch (InterruptedException | NullPointerException ex) {
            return result;
        }

        /*En java 7 sería:
        long now = System.currentTimeMillis();
        long timeoutInMillis = 1000L * timeoutInSeconds;
        long finish = now + timeoutInMillis;
        //espera a que termine el proceso o timeout
        while ( isAlive( p ) && ( System.currentTimeMillis() < finish ) ){ 
            Thread.sleep( 10 );
        }
        if ( isAlive( p ) ){
            return result;
        }
        BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
        StringBuilder builder = new StringBuilder();
        String line = null;
        while ((line = reader.readLine()) != null) {
        builder.append(line);
        builder.append(System.getProperty("line.separator"));
        result = builder.toString();
        return getInfo(result);
         */
    }

    public static String getDatos(Integer numero) throws IOException, InterruptedException {
        //String send = P4 + numero + P5;
        //return peticion(send);
        return getDatosParser(numero);
    }

    public static String getDatos(String Lat, String Lon) throws IOException, InterruptedException {
        String send = P1 + Lat + P2 + Lon + P3;
        return peticion(send);
    }
    private static final String URL_PARSER_BASE = "http://infobus.vitrasa.es:8002/Default.aspx?parada=";

    public static String getDatosParser(Integer number) throws IOException {
        String txt = "";
        try {
            URL url = new URL(URL_PARSER_BASE + number);
            BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
            String t;
            ArrayList<String> texts = new ArrayList<>();
            while ((t = br.readLine()) != null) {
                if (t.trim().startsWith("<td align=\"left\" width=\"15%\">")) {
                    // System.out.println(t);
                    t=t.trim().replace("<td align=\"left\" width=\"15%\"><font color=\"#333333\" size=\"3\">", "");
                    t=t.replace("</font></td><td align=\"left\" width=\"75%\"><font color=\"#333333\" size=\"3\">", ";;;");
                    t=t.replace("</font></td><td align=\"right\" width=\"10%\"><font color=\"#333333\" size=\"3\">", ";;;");
                    t=t.trim().replace("<td align=\"left\" width=\"15%\"><font color=\"#284775\" size=\"3\">", "");
                    t=t.replace("</font></td><td align=\"left\" width=\"75%\"><font color=\"#284775\" size=\"3\">", ";;;");
                    t=t.replace("</font></td><td align=\"right\" width=\"10%\"><font color=\"#284775\" size=\"3\">", ";;;");
                    t=t.replace("</font></td>", "");
                    t=StringEscapeUtils.unescapeHtml(t);
                     //System.out.println(t);
                    texts.add(t);
                }
            }
            br.close();
            String resultado = "";
            ArrayList<OrdenarRespuesta> al = new ArrayList<>();
            int cont = 0;
            for (int i = 0; i < texts.size(); i++) {
                if (cont < MAXIMOS_BUSES) {
                    String[] aux = texts.get(i).split(";;;");
                    String Linea = aux[0];
                    String nombre = aux[1];
                    String tiempo = aux[2];
                    al.add(new OrdenarRespuesta(Linea, nombre, tiempo));
                    cont++;
                } else {
                    System.out.println("paused");
                    break;
                }
            }
            Collections.sort(al);
            for (OrdenarRespuesta or : al) {
                resultado += or + "\n";
            }
            return resultado;
        } catch (MalformedURLException ex) {
            Logger.getLogger(Soap.class.getName()).log(Level.SEVERE, null, ex);
        }
        return txt;
    }
}
