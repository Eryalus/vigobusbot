/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataBase;

import datos.Contacto;
import datos.Parada;
import datos.ParadaFavorita;
import datos.Persona;
import datos.calculo_rutas.DatosRutaPersona;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.time.MyOwnCalendar;

/**
 * Utilidades básicas de la base de datos.
 *
 * @author ad_ri
 */
public class Basics {

    private static final String NOMBRE_DB = "vitrasa";
    private static final String IP = "localhost";
    private static final String SQL_PATH = "modelo.sql";

    /**
     * Trata de crear la base de datos en caso de no estar creada.
     *
     * @param usuario Usuario de la base de datos
     * @param pass Contraseña de a base de datos
     * @param port Puerto de acceso a la base de datos
     * @return true en caso de terminar la ejecución dejando la base de datos
     * creada o false en caso de haber algún error
     */
    public static boolean crearBaseDatos(String usuario, String pass, String port) {
        try {
            Connection conn = MySQLConnection(usuario, pass, port, "");
            importSQL(conn, new FileInputStream(SQL_PATH));
            conn.close();
        } catch (FileNotFoundException | ClassNotFoundException | SQLException | InstantiationException | IllegalAccessException ex) {
            ex.printStackTrace();
            return false;
        }

        return true;
    }

    public static void importSQL(Connection conn, InputStream in) throws SQLException {
        Scanner s = new Scanner(in);
        s.useDelimiter("(;(\r)?\n)|(--\n)");
        Statement st = null;
        try {
            st = conn.createStatement();
            while (s.hasNext()) {
                String line = s.next();
                if (line.startsWith("/*!") && line.endsWith("*/")) {
                    int i = line.indexOf(' ');
                    line = line.substring(i + 1, line.length() - " */".length());
                }

                if (line.trim().length() > 0) {
                    st.execute(line);
                }
            }
        } finally {
            if (st != null) {
                st.close();
            }
        }
    }

    public static boolean addAdmin(Long id, Connection conn) {
        try {
            Statement st = conn.createStatement();
            st.execute("INSERT INTO op VALUES(" + id + ")");
            return true;
        } catch (SQLException ex) {
            return false;
        }
    }

    public static boolean setTiempo_llegada(Long id, String tiempo, Connection conn) {
        try {
            Statement st = conn.createStatement();
            st.execute("UPDATE `personas` SET `tiempo_llegada` = '" + tiempo + "' WHERE `personas`.`id_chat` = " + id);
            return true;
        } catch (SQLException ex) {
            return false;
        }
    }

    public static boolean setUbicacion_origen(Long id, String ubicacion, Connection conn) {
        try {
            Statement st = conn.createStatement();
            st.execute("UPDATE `personas` SET `ubicacion_origen` = '" + ubicacion + "' WHERE `personas`.`id_chat` = " + id);
            return true;
        } catch (SQLException ex) {
            return false;
        }
    }

    public static boolean setUbicacion_destino(Long id, String ubicacion, Connection conn) {
        try {
            Statement st = conn.createStatement();
            st.execute("UPDATE `personas` SET `ubicacion_destino` = '" + ubicacion + "' WHERE `personas`.`id_chat` = " + id);
            return true;
        } catch (SQLException ex) {
            return false;
        }
    }

    public static DatosRutaPersona getDatosCalculoRuta(Long id, Connection conn) {
        try {
            Statement st = conn.createStatement();
            ResultSet result = st.executeQuery("SELECT ubicacion_origen,ubicacion_destino,tiempo_llegada FROM personas where id_chat=" + id);
            if (result.next()) {
                DatosRutaPersona datos = new DatosRutaPersona();
                datos.setTiempo(result.getString("tiempo_llegada"));
                datos.setUbicacion_destino(result.getString("ubicacion_destino"));
                datos.setUbicacion_origen(result.getString("ubicacion_origen"));
                return datos;
            }
        } catch (SQLException ex) {
        }
        return null;
    }

    public static boolean delPerson(Long id, Connection conn) {
        try {
            Statement st = conn.createStatement();
            st.execute("DELETE FROM op WHERE n_chat=" + id);
            st.execute("DELETE FROM log WHERE id_chat=" + id);
            st.execute("DELETE FROM contact WHERE id_chat=" + id);
            st.execute("UPDATE `contacto` SET `id_chat` = NULL WHERE `contacto`.`id_chat` = " + id);
            st.execute("DELETE FROM favoritos WHERE n_chat=" + id);
            st.execute("DELETE FROM personas WHERE id_chat=" + id);
            return true;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public static ArrayList<Contacto> getContact(Long id, Connection conn) {
        ArrayList<Contacto> al = new ArrayList<>();
        try {
            Statement st = conn.createStatement();
            ResultSet result = st.executeQuery("SELECT text,time FROM contacto where id_chat= " + id);
            while (result.next()) {
                al.add(new Contacto(result.getString("text"), result.getLong("time")));
            }
        } catch (SQLException ex) {
        }
        return al;
    }

    public static boolean delAdmin(Long id, Connection conn) {
        try {
            Statement st = conn.createStatement();
            st.execute("DELETE FROM op WHERE n_chat=" + id);
            return true;
        } catch (SQLException ex) {
            return false;
        }
    }

    public static ArrayList<Long> getContact(Connection conn) {
        ArrayList<Long> idchats = new ArrayList<>();
        try {
            Statement st = conn.createStatement();
            ResultSet result = st.executeQuery("SELECT id_chat FROM contact ");
            while (result.next()) {
                idchats.add(result.getLong("id_chat"));
            }
        } catch (SQLException ex) {
        }
        return idchats;
    }

    public static boolean updateUltimaParada(long id_chat, String infoparada, Connection conn) {
        try {
            Statement st = conn.createStatement();
            st.execute("update personas set ult_par='" + infoparada + "' where id_chat=" + id_chat);
            return true;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public static String getUltimaParada(long id_chat, Connection conn) {
        try {
            Statement st = conn.createStatement();
            ResultSet result = st.executeQuery("select * from personas where id_chat=" + id_chat);
            if (result.next()) {
                return result.getString("ult_par");
            }
        } catch (SQLException ex) {
        }
        return null;
    }

    public static boolean updateEstado(long id_chat, int estado, Connection conn) {
        try {
            Statement st = conn.createStatement();
            st.execute("update personas set estado=" + estado + " where id_chat=" + id_chat);
            return true;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public static Integer getEstado(long id_chat, Connection conn) {
        try {
            Statement st = conn.createStatement();
            ResultSet result = st.executeQuery("select * from personas where id_chat=" + id_chat);
            if (result.next()) {
                return result.getInt("estado");
            }
        } catch (SQLException ex) {
        }
        return null;
    }

    public static boolean updateLineaSolicitada(long id_chat, String linea, Connection conn) {
        try {
            Statement st = conn.createStatement();
            st.execute("update personas set linea_solicitada='" + linea + "' where id_chat=" + id_chat);
            return true;
        } catch (SQLException ex) {
        }
        return false;
    }

    public static String getLineaSolicitada(long id_chat, Connection conn) {
        try {
            Statement st = conn.createStatement();
            ResultSet result = st.executeQuery("select * from personas where id_chat=" + id_chat);
            if (result.next()) {
                return result.getString("linea_solicitada");
            }
        } catch (SQLException ex) {
        }
        return null;
    }

    public static boolean updateUltFav(long id_chat, int id_parada, Connection conn) {
        try {
            Statement st = conn.createStatement();
            st.execute("update personas set ult_fav=" + id_parada + " where id_chat=" + id_chat);
            return true;
        } catch (SQLException ex) {
        }
        return false;
    }

    public static Integer getUltFav(long id_chat, Connection conn) {
        try {
            Statement st = conn.createStatement();
            ResultSet result = st.executeQuery("select * from personas where id_chat=" + id_chat);
            if (result.next()) {
                return result.getInt("ult_fav");
            }
        } catch (SQLException ex) {
        }
        return null;
    }

    public static ArrayList<Long> getLog(Connection conn) {
        ArrayList<Long> idchats = new ArrayList<>();
        try {
            Statement st = conn.createStatement();
            ResultSet result = st.executeQuery("SELECT id_chat FROM log");
            while (result.next()) {
                idchats.add(result.getLong("id_chat"));
            }
        } catch (SQLException ex) {
        }
        return idchats;
    }

    public synchronized String getDump(String pass, String user, String host) {
        String output = null;
        try {
            Process p;
            if (pass.equals("")) {
                p = Runtime.getRuntime().exec("mysqldump -h " + host + " -u " + user + " vitrasa");
            } else {
                p = Runtime.getRuntime().exec("mysqldump -p" + pass + " -h " + host + " -u " + user + " vitrasa");
            }

            InputStream in = p.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            br.readLine();
            wait(1);

            String line;
            output = "";
            while ((line = br.readLine()) != null) {
                output += line + "\n";
            }
            in.close();
        } catch (IOException ex) {
        } catch (InterruptedException ex) {
            Logger.getLogger(Basics.class.getName()).log(Level.SEVERE, null, ex);
        }
        return output;
    }

    public static Long getIDbyUser(String user, Connection conn) {
        Long id = new Long(-1);
        try {
            Statement st = conn.createStatement();
            ResultSet result = st.executeQuery("SELECT id_chat FROM personas WHERE usuario='" + user + "'");
            while (result.next()) {
                id = result.getLong("id_chat");
                break;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return id;
    }

    public static String readInfo(String URL) throws MalformedURLException, IOException {
        String line;
        URL url = new URL(URL);
        URLConnection conexion = url.openConnection();
        conexion.setDoOutput(true);
        String ordinario = "";
        ArrayList<String> type = new ArrayList<>(), precio = new ArrayList<>();
        BufferedReader br = new BufferedReader(new InputStreamReader(conexion.getInputStream()));
        while ((line = br.readLine()) != null) {
            if (line.startsWith("<div style=\"text-align: left; margin-left: 40px;\">- <strong>TARIFA ORDINARIA</strong>: <span style=\"font-weight: bold;\">")) {
                ordinario = line.split("<div style=\"text-align: left; margin-left: 40px;\">- <strong>TARIFA ORDINARIA</strong>: <span style=\"font-weight: bold;\">")[1].split("&euro")[0].trim();
            } else if (line.startsWith("<div style=\"margin-left: 40px;\"><strong>- TARJETAS&nbsp;DE TRANSPORTE:</strong>&nbsp;</div>")) {
                br.readLine();
                br.readLine();
                String aux = "";
                while (!(aux = br.readLine()).startsWith("</ul>")) {
                    type.add(aux.split("<li>")[1].split(":")[0].trim());
                    precio.add(aux.split(";\">")[1].split("</")[0].replace("&euro;", "€").trim());
                }
                break;
            }
        }
        br.close();
        String ret = "<b>General:</b>\n" + ordinario + "€\n<b>Tarjeta:</b>\n";
        for (int i = 0; i < type.size(); i++) {
            ret += type.get(i) + ": " + precio.get(i) + "\n";
        }
        return ret;
    }

    public static void changeLastInteract(Connection conn, Long id) {
        try {
            Statement st = conn.createStatement();
            st.execute("UPDATE `personas` SET `lastinteract` = '" + new MyOwnCalendar().getTimeInMillis() + "' WHERE `personas`.`id_chat` = " + id + ";");
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public static ArrayList<Long> cargarAdmins(ReguladorConexion regcon, boolean first) {
        try {
            ArrayList<Long> admins = new ArrayList<>();
            Statement st = regcon.getConnection().createStatement();
            ResultSet result = st.executeQuery("SELECT * FROM op");
            while (result.next()) {
                admins.add(result.getLong("n_chat"));
            }
            return admins;
        } catch (SQLException ex) {
            regcon.refresh();
            if (first) {
                return cargarAdmins(regcon, false);
            }
            return new ArrayList<>();
        }
    }

    public static int[] actualizarParadas(ArrayList<Parada> paradas, Connection conn) {
        int actualizadas = 0;
        int añadidas = 0;
        for (Parada p : paradas) {
            try {
                Statement st = conn.createStatement();
                ResultSet res = st.executeQuery("SELECT * FROM paradas WHERE id=" + p.getNum());
                if (res.next()) {//existe
                    if (!res.getString("nombre").equals(p.getNombre()) || res.getDouble("latitud") != p.getLatitud()
                            || res.getDouble("longitud") != p.getLongitud()) {
                        System.out.println("Modificar:");
                        System.out.println("Parada: " + p.getNum());
                        System.out.println("Nombre orig: " + res.getString("nombre") + " Nombre mod: " + p.getNombre());
                        System.out.println("Latitud orig: " + res.getDouble("latitud") + " Latitud mod: " + p.getLatitud());
                        System.out.println("Longitud orig: " + res.getDouble("longitud") + " Nombre mod: " + p.getLongitud());
                        System.out.println("Altura orig: " + res.getString("altura") + " Nombre mod: " + p.getAltitud());
                        System.out.println();
                        st.execute("UPDATE paradas SET longitud=" + p.getLongitud() + ",latitud=" + p.getLatitud()
                                + ",nombre='" + p.getNombre() + "',altura='" + p.getAltitud() + "' WHERE id=" + p.getNum());
                        actualizadas++;
                    }
                } else {
                    System.out.println("Añadir:");
                    System.out.println("Parada: " + p.getNum());
                    System.out.println("Nombre: " + p.getNombre());
                    System.out.println("Latitud: " + p.getLatitud());
                    System.out.println("Longitud: " + p.getLongitud());
                    System.out.println("Altura: " + p.getAltitud());
                    System.out.println();
                    st.execute("INSERT INTO paradas VALUES(" + p.getNum()
                            + ",'" + p.getNombre() + "'," + p.getLatitud() + "," + p.getLongitud()
                            + ",'" + p.getAltitud() + "')");
                    añadidas++;
                }
            } catch (SQLException ex) {
                System.err.println("Error: " + p);
            }

        }
        int[] ret = new int[2];
        ret[0] = actualizadas;
        ret[1] = añadidas;
        return ret;
    }

    public static int[] actualizarParadas(HashMap<Integer, Parada> paradas, Connection conn) {
        Set<Integer> set = paradas.keySet();
        int actualizadas = 0;
        int añadidas = 0;
        for (Integer n : set) {
            Parada p = paradas.get(n);
            try {
                Statement st = conn.createStatement();
                ResultSet res = st.executeQuery("SELECT * FROM paradas WHERE id=" + n);
                if (res.next()) {//existe
                    st.execute("UPDATE paradas SET longitud=" + p.getLongitud() + ",latitud=" + p.getLatitud()
                            + ",nombre='" + p.getNombre() + "',altura='" + p.getAltitud() + "' WHERE id=" + n);
                    actualizadas++;
                } else {
                    st.execute("INSERT INTO paradas VALUES(" + n
                            + ",'" + p.getNombre() + "'," + p.getLatitud() + "," + p.getLongitud()
                            + ",'" + p.getAltitud() + "')");
                    añadidas++;
                }
            } catch (SQLException ex) {
                System.err.println("Error: " + p);
            }

        }
        int[] ret = new int[2];
        ret[0] = actualizadas;
        ret[1] = añadidas;
        return ret;
    }

    public static void delFavorita(Long chat_id, int parada, Statement st) throws SQLException {
        st.execute("DELETE FROM favoritos WHERE n_parada=" + parada + " AND n_chat=" + chat_id);
    }

    public static ArrayList<Integer> getFavoritas(Long chat_id, Statement st) throws SQLException {
        ArrayList<Integer> al = new ArrayList<>();
        ResultSet res = st.executeQuery("SELECT * FROM favoritos WHERE n_chat=" + chat_id);
        while (res.next()) {
            al.add(res.getInt("n_parada"));
        }
        return al;
    }

    public static ArrayList<ParadaFavorita> getParadasFavoritas(Long chat_id, Statement st) throws SQLException {
        ArrayList<ParadaFavorita> al = new ArrayList<>();
        ResultSet res = st.executeQuery("SELECT * FROM favoritos WHERE n_chat=" + chat_id);
        while (res.next()) {
            int id = res.getInt("n_parada");
            String alias = res.getString("alias");
            al.add(new ParadaFavorita(id, alias));
        }
        return al;
    }

    public static void addFavorita(Long id, ParadaFavorita parada, Statement st) throws SQLException {
        ResultSet res = st.executeQuery("SELECT * FROM favoritos WHERE n_parada=" + parada.getFavoritaI() + " AND n_chat=" + id);
        if (!res.next()) {
            st.execute("INSERT INTO favoritos VALUES(" + id + "," + parada.getFavoritaI() + ",'" + parada.getAlias() + "' )");

        }
    }

    public static HashMap<Integer, Parada> cargarParadas(Connection conn, boolean verbose) {
        HashMap<Integer, Parada> map = new HashMap<>();
        try {
            if (verbose) {
                System.out.println("Cargando datos de paradas...");
            }
            Statement st = conn.createStatement();
            ResultSet res = st.executeQuery("SELECT * FROM paradas");
            int num = 0;
            while (res.next()) {
                Integer numero = res.getInt("id");
                if (numero >= 0) {
                    num++;
                    double latitud = res.getDouble("latitud");
                    double longitud = res.getDouble("longitud");
                    String nombre = res.getString("nombre");
                    String alt = res.getString("altura");
                    Parada p = new Parada(numero, latitud, longitud, alt, nombre);
                    map.put(numero, p);
                }
            }
            if (verbose) {
                switch (num) {
                    case 0:
                        System.out.println("No hay ninguna parada almacenada");
                        break;
                    case 1:
                        System.out.println("Se ha cargado una parada");
                        break;
                    default:
                        System.out.println("Se han cargado " + num + " paradas");
                        break;

                }
            }
        } catch (Exception ex) {
            if (verbose) {
                System.err.println("No se han podido cargar los datos de paradas");
                System.err.println("Saliendo");
                System.exit(0);
            }
        }
        return map;
    }

    public static HashMap<Integer, Parada> cargarParadas(Connection conn, String pattern) {
        HashMap<Integer, Parada> map = new HashMap<>();
        try {
            System.out.println("Cargando datos de paradas...");
            Statement st = conn.createStatement();
            ResultSet res = st.executeQuery("SELECT * FROM paradas WHERE nombre LIKE '" + pattern + "' or id LIKE '" + pattern + "'");
            while (res.next()) {
                Integer numero = res.getInt("id");
                if (numero >= 0) {
                    double latitud = res.getDouble("latitud");
                    double longitud = res.getDouble("longitud");
                    String nombre = res.getString("nombre");
                    String alt = res.getString("altura");
                    Parada p = new Parada(numero, latitud, longitud, alt, nombre);
                    map.put(numero, p);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return map;
    }

    public static void addPersona(Persona p, Connection conn) {
        try {
            Statement st = conn.createStatement();
            st.executeUpdate("INSERT INTO personas(id_chat,usuario,nombre,apellidos,lastinteract) VALUES(" + p.getID() + ",'" + p.getUserName() + "','" + p.getNombre() + "','" + p.getApellidos() + "',0)");
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public static void updatePersona(Persona p, Connection conn) {
        try {
            Statement st = conn.createStatement();
            st.executeUpdate("update personas set nombre='" + p.getNombre() + "' where id_chat=" + p.getID());
            st.executeUpdate("update personas set apellidos='" + p.getApellidos() + "' where id_chat=" + p.getID());
            st.executeUpdate("update personas set usuario='" + p.getUserName() + "' where id_chat=" + p.getID());
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public static void addContacta(String txt, long chat_id, Connection conn) {
        try {
            Statement st = conn.createStatement();
            st.executeUpdate("insert contacto(text,id_chat,time) values('" + txt + "'," + chat_id + "," + new MyOwnCalendar().getTimeInMillis() + ")");
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Carga el listado de usuarios que estan en la base de datos
     *
     * @param conn Connection con la base de datos
     * @return HashMap {Long (id del chat),Persona} de los usuarios
     */
    public static HashMap<Long, Persona> cargarPersonas(Connection conn, boolean verbose) {
        HashMap<Long, Persona> map = new HashMap<>();
        try {
            if (verbose) {
                System.out.println("Cargando datos de usuarios...");
            }
            Statement st = conn.createStatement();
            ResultSet res = st.executeQuery("SELECT * FROM personas");
            int num = 0;
            while (res.next()) {
                Long id = res.getLong("id_chat");
                if (id >= 0) {
                    num++;
                    String user = res.getString("usuario");
                    String nombre = res.getString("nombre");
                    String ape = res.getString("apellidos");
                    //cargamos las paradas favoritas
                    HashMap<Integer, ParadaFavorita> bss = new HashMap<>();
                    try {
                        ResultSet ress = conn.createStatement().executeQuery("SELECT * FROM favoritos WHERE n_chat=" + id);
                        while (ress.next()) {
                            Integer numero = ress.getInt("n_parada");
                            String alias = ress.getString("alias");
                            bss.put(numero, new ParadaFavorita(numero, alias));
                        }
                    } catch (SQLException ex) {
                    }
                    Persona p = new Persona(id, user, nombre, ape, bss);
                    map.put(id, p);
                }
            }
            if (verbose) {
                switch (num) {
                    case 0:
                        System.out.println("No hay ningun usuario almacenado");
                        break;
                    case 1:
                        System.out.println("Se ha cargado un usuario");
                        break;
                    default:
                        System.out.println("Se han cargado " + num + " usuarios");
                        break;

                }
            }
        } catch (NullPointerException ex) {
            ex.printStackTrace();
        } catch (Exception ex) {
            if (verbose) {
                System.err.println("No se han podido cargar los datos de usuario");
                System.err.println("Saliendo");
                System.exit(0);
            }
        }
        return map;
    }

    public static Persona getPersona(Long id_chat, Connection conn) {
        try {
            Statement st = conn.createStatement();
            ResultSet res = st.executeQuery("SELECT * FROM personas where id_chat=" + id_chat);
            int num = 0;
            while (res.next()) {
                Long id = res.getLong("id_chat");
                if (id >= 0) {
                    num++;
                    String user = res.getString("usuario");
                    String nombre = res.getString("nombre");
                    String ape = res.getString("apellidos");
                    //cargamos las paradas favoritas
                    HashMap<Integer, ParadaFavorita> bss = new HashMap<>();
                    try {
                        ResultSet ress = conn.createStatement().executeQuery("SELECT * FROM favoritos WHERE n_chat=" + id);
                        while (ress.next()) {
                            Integer numero = ress.getInt("n_parada");
                            String alias = ress.getString("alias");
                            bss.put(numero, new ParadaFavorita(numero, alias));
                        }
                    } catch (SQLException ex) {
                    }
                    Persona p = new Persona(id, user, nombre, ape, bss);
                    return p;
                }
            }

        } catch (NullPointerException ex) {

        } catch (Exception ex) {

        }
        return null;
    }

    /**
     * Crea una conexión con la base de datos a nivel de la DB del programa
     *
     * @return Conexión con la base de datos ex1.printStackTrace();
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public static Connection conectarBaseDatos(String usuario, String pass, String port) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        return MySQLConnection(usuario, pass, port, NOMBRE_DB);
    }

    /**
     * Crea una conexión con la base de datos dada.
     *
     * @param user usuario
     * @param pass contraseña
     * @param db_name nombre de la base de datos a conectar
     * @return Conexión
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    private static Connection MySQLConnection(String user, String pass, String port, String db_name) throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
        Class.forName("com.mysql.jdbc.Driver").newInstance();
        Connection conn = null;
        if (port.equals("")) {
            conn = DriverManager.getConnection("jdbc:mysql://" + IP + "/" + db_name + "?connectTimeout=15000", user, pass);
        } else {
            conn = DriverManager.getConnection("jdbc:mysql://" + IP + ":" + port + "/" + db_name + "?connectTimeout=15000", user, pass);
        }
        return conn;
    }
}
