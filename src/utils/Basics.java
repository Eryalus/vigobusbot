/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import utils.time.MyOwnCalendar;
import datos.Parada;
import java.io.IOException;
import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;

/**
 *
 * @author eryalus
 */
public class Basics {

    public static ArrayList<SendMessage> getDataInMessages(String t, ArrayList<SendMessage> ms, Integer numero, Parada p) throws IOException, InterruptedException {
        SendMessage m = new SendMessage();
        String temp = new MyOwnCalendar().toString();
        String time = temp.substring(1, temp.lastIndexOf("-"));
        temp = "<b>" + p + "</b>\n" + time + "\n\n";
        String[] lineas = t.split("\n");
        for (String linea : lineas) {
            if ((temp.length() + linea.length()) <= 4000) {
                temp += linea + "\n";
            } else {
                m = new SendMessage();
                m.setText(temp);
                temp = linea + "\n";
                ms.add(m);
            }
        }
        m = new SendMessage();
        m.setText(temp);
        m.setReplyMarkup(setInline("reload-" + numero, "Recargar"));
        ms.add(m);
        return ms;
    }

    public static InlineKeyboardMarkup setInline(String txt, String txt_to_user) {
        InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
        List<InlineKeyboardButton> rowInline = new ArrayList<>();
        InlineKeyboardButton but = new InlineKeyboardButton();
        but.setCallbackData(txt);
        but.setText(txt_to_user);
        rowInline.add(but);
        rowsInline.add(rowInline);
        // Add it to the message
        markupInline.setKeyboard(rowsInline);
        return markupInline;
    }

    public static Integer minutosPorLinea(String linea) {
        Integer num = -1;
        try {
            num = Integer.parseInt(linea.split("<b>")[1].split("</b>")[0]);
        } catch (Exception ex) {

        }
        return num;
    }

    public static String getIP() {
        try {
            return Inet4Address.getLocalHost().getHostAddress() + " - " + Inet4Address.getLocalHost().getHostName();
        } catch (UnknownHostException ex) {
            return "No disponible";
        }
    }

    public static ArrayList<SendMessage> getIPMessage(ArrayList<SendMessage> ms) {
        SendMessage m = new SendMessage();
        String line;
        line = "IP: " + getIP() + "\n";

        m.setText(line);
        ms.add(m);
        return ms;
    }

    public static boolean isAlive(Process p) {
        try {
            p.exitValue();
            return false;
        } catch (IllegalThreadStateException e) {
            return true;
        }
    }

    public static String getFirstNum(String line) {
        String ac = "";
        for (int i = 0; i < line.length(); i++) {
            if (isDigit(line.charAt(i))) {
                ac += line.charAt(i);
            } else {
                break;
            }
        }
        if (ac.equals("")) {
            return null;
        }
        return ac;
    }

    private static boolean isDigit(char c) {
        switch (c) {
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                return true;
            default:
                return false;
        }
    }
}
