/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import utils.time.MyOwnCalendar;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 *
 * @author eryalus
 */
public class Log {

    public static String getBaseRute() {
        return System.getProperty("user.dir") + "/logs/";
    }

    public static String Log(String s) {
        if (s != null) {
            try {
                new File(getBaseRute()).mkdirs();
                String ruta = getBaseRute() + new MyOwnCalendar().getDate(MyOwnCalendar.SPANISH).replace("/", "-") + ".log";
                BufferedWriter bw = new BufferedWriter(new FileWriter(ruta, true));
                bw.write(s + "\n");
                bw.close();
            } catch (Exception ex) {

            }
        }
        return s;
    }

    public static File getAll() throws FileNotFoundException, IOException {
        String name = "logger" + Math.random() * 50000;
        try {
            ZipOutputStream os = new ZipOutputStream(new FileOutputStream(getBaseRute() + name + ".zip"));
            File file = new File(getBaseRute());
            FileFilter ff = new FileFilter() {
                @Override
                public boolean accept(File pathname) {
                    return pathname.getName().endsWith(".log");
                }
            };
            File[] logs = file.listFiles(ff);
            if (logs.length == 0) {
                return null;
            }
            for (File log : logs) {
                ZipEntry entrada = new ZipEntry(log.getName());
                os.putNextEntry(entrada);
                FileInputStream fis = new FileInputStream(log.getAbsolutePath());
                byte[] buffer = new byte[1024];
                int leido = 0;
                while (0 < (leido = fis.read(buffer))) {
                    os.write(buffer, 0, leido);
                }
                fis.close();
                os.closeEntry();
            }
            os.close();
            return new File(getBaseRute() + name + ".zip");
        } catch (Exception ex) {
            File f = new File(getBaseRute() + name + ".zip");
            f.delete();
            throw ex;
        }
    }

}
