/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import datos.Parada;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author eryalus
 */
public class ConexionPaginas {

    public static ArrayList<Parada> cargarParadasFromVitrasa(String url) {
        ArrayList<Parada> al = new ArrayList<>();
        try {
            URL ur = new URL(url);
            InputStream in = ur.openStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String temp;
            while ((temp = br.readLine()) != null) {
                if (temp.startsWith("puntoParada = new google.maps.LatLng")) {
                    String[] partes = temp.split("puntoParada = new google.maps.LatLng");
                    for (String p : partes) {
                        String[] partes2 = p.split(";");
                        String nombre;
                        double lat, lon;
                        int num = 0;
                        if (partes2.length == 3) {
                            try {
                                partes2[0] = partes2[0].replace("(", "").replace(")", "");
                                String[] latlon = partes2[0].split(",");
                                if (latlon.length == 2) {
                                    lat = Double.parseDouble(latlon[0]);
                                    lon = Double.parseDouble(latlon[1]);
                                } else {
                                    continue;
                                }
                                String[] partes3 = partes2[1].split("<strong>");
                                if (partes3.length == 2) {
                                    String[] partes4 = partes3[1].split("</strong>");
                                    if (partes4.length == 2) {
                                        nombre = partes4[0].replace("\\'", "'");;
                                        String[] partes5 = partes4[1].split("/>N");
                                        if (partes5.length == 2) {
                                            String[] partes6 = partes5[1].split("\\.");
                                            String[] partes7 = partes6[0].split(":");
                                            if (partes7.length == 2) {
                                                String n = partes7[1].trim();
                                                num = Integer.parseInt(n);
                                            }
                                        } else {
                                            continue;
                                        }
                                    } else {
                                        continue;
                                    }
                                } else {
                                    continue;
                                }
                            } catch (Exception ex) {
                                continue;
                            }
                            Parada par = new Parada(num, lat, lon, "0", nombre);
                            al.add(par);
                        }
                    }
                }
            }
            br.close();
        } catch (IOException ex) {
            Logger.getLogger(ConexionPaginas.class.getName()).log(Level.SEVERE, null, ex);
        }
        return al;
    }

    public static ArrayList<Parada> cargarParadasHTML(String ruta) {
        ArrayList<Parada> al = new ArrayList<>();
        BufferedReader br;
        String line;
        try {
            br = new BufferedReader(new FileReader(ruta));

            while ((line = br.readLine()) != null) {
                if (line.contains("puntoParada = new google.maps.LatLng")) {
                    line = line.replace("(", "");
                    String[] par = line.split("puntoParada = new google.maps.LatLng");
                    for (int i = 1; i < par.length; i++) {
                        String[] info = par[i].split(";");
                        try {
                            String nombre = info[3].split("&lt")[0];
                            //nombre = nombre.substring(0,nombre.length()-3);
                            String tem = info[7].replace(" ", "").split(":")[1];
                            String numero = utils.Basics.getFirstNum(tem);

                            String lat = info[0].split(",")[0].trim();
                            String lon = info[0].split(",")[1].trim().replace(")", "");
                            String altura = info[12].split("Altura=")[1].split("&")[0];
                            double latd = Double.parseDouble(lat);
                            double lond = Double.parseDouble(lon);
                            int num = Integer.parseInt(numero);
                            Parada temp_p = new Parada(num, latd, lond, altura, nombre);
                            al.add(temp_p);
                        } catch (Exception ex) {

                        }

                    }
                    break;
                }
            }
        } catch (MalformedURLException ex) {
            System.err.println("Ruta inválida");
            System.out.println("Ruta>");
        } catch (IOException ex) {
            ex.printStackTrace();
            System.err.println("Error leyendo el fichero");
            System.out.println("Ruta>");
        }

        return al;
    }

    public static String descargarParada(double lat, double lon, String alt) {
        URL url;
        InputStream is = null;
        BufferedReader br;
        String line;
        String temp = "";
        try {
            url = new URL("http://rutas.vitrasa.es/DisplayParadas.aspx?LatitudParada=" + lat
                    + "&LongitudParada=" + lon + "&Altura=" + alt + "&Zoom=");
            is = url.openStream();  // throws an IOException
            br = new BufferedReader(new InputStreamReader(is));

            while ((line = br.readLine()) != null) {
                if (line.startsWith("infoWindowParada.setContent")) {
                    String[] buses = line.split("<tr class=\"filaimpar\">");
                    if (buses.length == 1) {
                        buses = line.replace("'", "`").split("<tr class=\"filapar\">");
                    }
                    for (int i = 1; i < buses.length; i++) {
                        String[] info = buses[i].split(">");
                        String Bus = info[1].split("<")[0];
                        String Nombre = info[3].split("<")[0];
                        String tiempo = info[5].split("<")[0];
                        temp += Bus + " " + Nombre + " -> <b>" + tiempo + "</b> minutos\n";

                    }
                    break;
                }
            }

        } catch (MalformedURLException mue) {
            mue.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } finally {
            try {
                if (is != null) {
                    is.close();
                }
            } catch (IOException ioe) {
                // nothing to see here
            }
        }
        if (temp.equals("")) {
            temp = "No hay datos de llegadas";
        }
        return temp;
    }
}
