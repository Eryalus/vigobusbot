/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comprobaciones;

import datos.InfoActualizaciones;
import principal.BotTelegram;
import utils.time.Timer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 *
 * @author eryalus
 */
public class HiloActualizacionParadas extends Timer {

    private HashMap<Long, ArrayList<InfoActualizaciones>> map = new HashMap<>();
    private final BotTelegram parent;
    private ArrayList<Object[]> aborrar = new ArrayList();

    /**
     * Crea un timer
     *
     * @param tiempo intervalo de espera entre ejecución y ejecución en
     * milisegundos
     * @param parent Bot de telegram para enviar las respuestas
     */
    public HiloActualizacionParadas(Long tiempo, BotTelegram parent) {
        super(tiempo);
        this.parent = parent;
    }

    
    
   
    
    protected void setRestante(Long id, int index, int restante) {
        map.get(id).get(index).setRestante(restante);
    }

    public synchronized void borrarInfo(Long chat_id, InfoActualizaciones info) {
        aborrar.add(new Object[]{chat_id, info});
    }

    public HashMap<Long, ArrayList<InfoActualizaciones>> getMap() {
        return map;
    }

    public ArrayList<InfoActualizaciones> getActualizaciones(Long chat_id) {
        ArrayList<InfoActualizaciones> al = map.get(chat_id);
        if (al == null) {
            al = new ArrayList<>();
        }
        return al;
    }

    /**
     * Borra las que ya se han terminado y lanza los hilos que hacen las
     * peticiones restantes
     */
    @Override
    protected void action() {
        //Borra las que ya se han encontrado
        for (Object[] ob : aborrar) {
            Long chat_id = (Long) ob[0];
            InfoActualizaciones info = (InfoActualizaciones) ob[1];
            map.get(chat_id).remove(info);
        }
        aborrar = new ArrayList<>();

        //ahora creamos un hilo que pida info al servidor y avise al
        //usuario si se cumple el criterio
        Set<Long> keys = map.keySet();
        for (Long key : keys) {
            ArrayList<InfoActualizaciones> al = map.get(key);
            for (int i = 0; i < al.size(); i++) {
                CompruebaLlegadaBus comp = new CompruebaLlegadaBus(key, map, i, parent, this);
                comp.start();
            }
        }
    }

    /**
     * Añade una parada a la espera.
     *
     * @param parada Numero de parada
     * @param bus Linea de bus
     * @param tiempo Minutos restantes para avisarle
     * @param chat_id ID de chat a responder
     * @return true en caso de añadirlo correctamente false en caso de ya estar
     * en seguimiento
     */
    public boolean addParada(Integer parada, String bus, Integer tiempo, Long chat_id) {
        ArrayList<InfoActualizaciones> al = map.get(chat_id);
        if (al == null) {
            al = new ArrayList<>();
            al.add(new InfoActualizaciones(parada, bus, tiempo));
            map.put(chat_id, al);
            return true;
        } else {
            InfoActualizaciones info = new InfoActualizaciones(parada, bus, tiempo);
            boolean temp = false;
            for (InfoActualizaciones t : al) {
                if (t.equals(info)) {
                    temp = true;
                    break;
                }
            }
            if (temp) {//al.contains(info)) {
                //ya está
                return false;
            } else {
                //no está, lo añade
                al.add(info);
                return true;
            }
        }
    }

}
