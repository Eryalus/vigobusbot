/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comprobaciones;

import dataBase.Soap;
import datos.InfoActualizaciones;
import principal.BotTelegram;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.exceptions.TelegramApiException;

/**
 *
 * @author eryalus
 */
public class CompruebaLlegadaBus extends Thread {

    private Thread hilo = null;
    private final BotTelegram bot;
    private final Long chat_id;
    private final HiloActualizacionParadas parent;
    private final Integer index;
    HashMap<Long, ArrayList<InfoActualizaciones>> map;

    public CompruebaLlegadaBus(Long chat_id, HashMap<Long, ArrayList<InfoActualizaciones>> map, Integer index, BotTelegram bot, HiloActualizacionParadas parent) {
        this.map = map;
        this.bot = bot;
        this.chat_id = chat_id;
        this.parent = parent;
        this.index = index;
    }

    public void run() {
        try {
            //System.out.println(this.act.getLinea()+"/"+this.act.getParada()
            //+"/"+this.act.getTiempo()+"/"+this.chat_id);
            String info = Soap.getDatos(map.get(chat_id).get(index).getParada());
            String[] lineas = info.split("\n");
            for (String lin : lineas) {
                if (lin.startsWith(map.get(chat_id).get(index).getLinea())) {
                    String t = lin.split("<b>")[1].split("</b>")[0];
                    Integer tem = Integer.parseInt(t);
                    if (map.get(chat_id).get(index).getTiempo() >= tem) {
                        SendMessage m = new SendMessage();
                        m.setChatId(chat_id);
                        if (map.get(chat_id).get(index).getTiempo() <= 3) {
                            m.setText("El bus \"" + map.get(chat_id).get(index).getLinea() + "\" llegará en " + tem + " minutos a la parada " + map.get(chat_id).get(index).getParada()+"\n\n<b>Recuerda que el vitrasa no te espera</b>");
                        } else {
                            m.setText("El bus \"" + map.get(chat_id).get(index).getLinea() + "\" llegará en " + tem + " minutos a la parada " + map.get(chat_id).get(index).getParada());
                        }
                        m.enableHtml(true);
                        bot.sendMessage(m);
                        parent.borrarInfo(chat_id, map.get(chat_id).get(index));
                    } else {
                        parent.setRestante(chat_id, index, tem);
                    }
                    break;
                }
            }
        } catch (IOException | InterruptedException | TelegramApiException ex) {
            Logger.getLogger(CompruebaLlegadaBus.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void start() {
        if (hilo == null) {
            hilo = new Thread(this, "Hilo comprobacion parada");
            hilo.start();
        }
    }
}
