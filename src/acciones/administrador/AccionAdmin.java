/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acciones.administrador;

import datos.Persona;
import estados.EstadoEnviarAdmin;
import estados.EstadoGeneralAdmin;
import principal.BotTelegram;
import principal.RespuestaConsulta;
import utils.time.MyOwnCalendar;
import utils.Log;
import acciones.Action;
import dataBase.Basics;
import estados.Estado;
import java.util.ArrayList;
import java.util.Calendar;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.exceptions.TelegramApiException;

/**
 *
 * @author eryalus
 */
public class AccionAdmin implements Action {

    private final Update UPDATE;
    private final BotTelegram PARENT;
    private String texto_log = "", error = "";
    private final RespuestaConsulta ant;

    public AccionAdmin(Update UPDATE, BotTelegram PARENT, RespuestaConsulta ant) {
        this.UPDATE = UPDATE;
        this.PARENT = PARENT;
        this.ant = ant;
    }

    @Override
    public boolean action() {
        Message mensaje = UPDATE.getMessage();
        String text = "";
        if (mensaje.hasText()) {
            text = mensaje.getText();
        }
        Long id = mensaje.getChatId();
        Basics.changeLastInteract(PARENT.getConnection(), id);
        Persona per = dataBase.Basics.getPersona(id, PARENT.getConnection());
        texto_log += "Inicio - " + (new MyOwnCalendar()).toString() + "\n";
        if (per != null) {
            texto_log += per + ";" + Calendar.getInstance().getTimeInMillis() + "\n";
        } else {
            texto_log += "Persona desconocida \"" + id + "\";" + Calendar.getInstance().getTimeInMillis() + "\n";
        }
        ArrayList<SendMessage> ms = new ArrayList<>();
        Integer estado = PARENT.getEstados(id);
        if (id < 0) {
            estado = 0;
        }
        if (estado != null) {
            switch (estado) {
                case Estado.ESTADO_GENERAL_ADMIN:
                    ms = new EstadoGeneralAdmin(text, id, ant, PARENT).response(ms);
                    break;
                case Estado.ESTADO_EVNIAR_ADMIN:
                    ms = new EstadoEnviarAdmin(text, id, ant, PARENT).response(ms);
                    break;
                default:
                    ms = new ArrayList<>();
                    break;
            }
        }
        if (ms.isEmpty()) {
            return false;
        }

        try {
            for (SendMessage mm : ms) {
                mm.setChatId(id);
                mm.setParseMode("html");
                PARENT.sendMessage(mm);
                MyOwnCalendar f = new MyOwnCalendar();
                //System.out.println(f.toString() + PARENT.getMap().get(id));
            }
        } catch (TelegramApiException ex) {
            error = ant.error(ex);
            System.out.println("Error enviando: " + dataBase.Basics.getPersona(id, PARENT.getConnection()));
        }
        if (!error.equals("")) {
            texto_log += "Texto:" + text + "\n" + error + "\n";
        }
        texto_log += "Fin - " + (new MyOwnCalendar()).toString();
        if (PARENT.isFullLog() || !error.equals("")) {
            System.out.println(Log.Log(texto_log));
        }
        return true;
    }

}
