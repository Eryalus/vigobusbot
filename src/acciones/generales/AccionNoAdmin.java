/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acciones.generales;

import datos.InfoParada;
import datos.Persona;
import estados.EstadoBuscar;
import estados.EstadoGeneral;
import estados.EstadoGeo;
import estados.EstadoParadaAñadir;
import estados.EstadoParadaAñadirNombre;
import estados.EstadoParadaBorrar;
import estados.EstadoPonerNombre;
import estados.EstadoTiempoParada;
import principal.BotTelegram;
import principal.RespuestaConsulta;
import utils.time.MyOwnCalendar;
import utils.Log;
import acciones.Action;
import static comandos.MessageEditor.removeTeclado;
import dataBase.Basics;
import estados.Estado;
import estados.EstadoContacta;
import estados.EstadoGetTimeCalculoRuta;
import estados.EstadoUbicacionDestino;
import estados.EstadoUbicacionOrigen;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Chat;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.exceptions.TelegramApiException;

/**
 *
 * @author eryalus
 */
public class AccionNoAdmin implements Action {

    private final Update UPDATE;
    private final BotTelegram PARENT;
    private String texto_log = "", error = "";
    private final RespuestaConsulta ant;

    public AccionNoAdmin(Update UPDATE, BotTelegram PARENT, RespuestaConsulta ant) {
        this.UPDATE = UPDATE;
        this.PARENT = PARENT;
        this.ant = ant;
    }

    @Override
    public boolean action() {
        if (UPDATE.getMessage().getChat().isUserChat()) {
            Message mensaje = UPDATE.getMessage();
            Chat chat = mensaje.getChat();
            Long id = chat.getId();
            Persona per = dataBase.Basics.getPersona(id, PARENT.getConnection());
            if (per == null) {
                String user = chat.getUserName();
                String first = chat.getFirstName();
                String last = chat.getLastName();
                per = new Persona(id, user, first, last, new HashMap<>());
                dataBase.Basics.addPersona(per, PARENT.getConnection());
                PARENT.putEstado(id, 0);
            } else {
                Persona tmp = new Persona(per.getID(), chat.getUserName(), chat.getFirstName(), chat.getLastName(), per.getFavs());
                per = tmp;
                dataBase.Basics.updatePersona(per, PARENT.getConnection());
            }
            Basics.changeLastInteract(PARENT.getConnection(), id);
            if (texto_log.equals("")) {
                texto_log += "Inicio - " + (new MyOwnCalendar()).toString() + "\n" + per + ";" + "\n";
            }
            Integer estado = PARENT.getEstados(id);
            String texto = "";
            if (mensaje.hasText()) {
                texto = mensaje.getText();
            }
            //System.out.println(estado);
            ArrayList<SendMessage> ms = new ArrayList<>();
            if (mensaje.hasLocation()) {
                if (estado != Estado.ESTADO_UBICACION_ORIGEN && estado != Estado.ESTADO_UBICACION_DESTINO) {
                    ms = new EstadoGeo(UPDATE, PARENT).response(ms);
                    estado = null;
                }

            }
            if (estado != null) {
                switch (estado) {
                    case Estado.ESTADO_GENERAL:
                        //general, el inicio
                        EstadoGeneral est = new EstadoGeneral(per, id, texto, ant, PARENT);
                        ms = est.response(ms);
                        Integer parada = est.getParada();
                        ArrayList<String> lista = est.getLista();
                        if (parada != null && !ms.isEmpty()) {
                            PARENT.putUltima_parada(id, new InfoParada(parada, est.getTexto()).setLlegadas(lista));
                        }
                        //System.out.println("Datos: " + parada + " " + est.getTexto());
                        break;
                    case Estado.ESTADO_TIEMPO_PARADA:
                        //introduce el tiempo para una parada
                        ms = new EstadoTiempoParada(id, texto, PARENT).response(ms);
                        break;
                    case Estado.ESTADO_PARADA_BORRAR:
                        //pide el numero de parada a eliminar
                        ms = new EstadoParadaBorrar(per, id, texto, ant, PARENT).response(ms);
                        break;
                    case Estado.ESTADO_PARADA_ADD:
                        //pide numero de parada a añadir a favs
                        ms = new EstadoParadaAñadir(id, texto, ant, PARENT).response(ms);
                        break;
                    case Estado.ESTADO_PONER_NOMBRE:
                        //mira si quiere ponerle nombre o no
                        ms = new EstadoPonerNombre(id, texto, PARENT).response(ms);
                        break;
                    case Estado.ESTADO_PARADA_ADD_NOMBRE:
                        //introduce un alias para la ultima parada fav
                        ms = new EstadoParadaAñadirNombre(id, texto, ant, PARENT).response(ms);
                        break;
                    case Estado.ESTADO_GEO:
                        ms = new EstadoGeo(UPDATE, PARENT).response(ms);
                        break;
                    case Estado.ESTADO_BUSCAR:
                        ms = new EstadoBuscar(id, texto, PARENT).response(ms);
                        break;
                    case Estado.ESTADO_CONTACTA:
                        ms = new EstadoContacta(id, texto, PARENT).response(ms);
                        break;
                    case Estado.ESTADO_GET_TIME_CALCULO_RUTA:
                        ms = new EstadoGetTimeCalculoRuta(id, texto, PARENT).response(ms);
                        break;
                    case Estado.ESTADO_UBICACION_ORIGEN:
                        ms = new EstadoUbicacionOrigen(UPDATE, PARENT).response(ms);
                        break;
                    case Estado.ESTADO_UBICACION_DESTINO:
                        ms = new EstadoUbicacionDestino(UPDATE, PARENT).response(ms);
                        break;
                    default:
                        //se supone y espero que no se de el caso (volvería al inicio
                        PARENT.putEstado(id, Estado.ESTADO_GENERAL);
                        SendMessage m = new SendMessage();
                        m.setText("Ha ocurrido un error con su solicitud");
                        ms.add(m);
                        break;
                }
            }

            if (ms.isEmpty()) {
                PARENT.putEstado(id, Estado.ESTADO_GENERAL);
                SendMessage m = new SendMessage();
                m.setText("Se ha producido un error");
                m = removeTeclado(m);
                ms.add(m);
            }
            for (SendMessage m : ms) {
                m.setChatId(id);
                try {
                    m.setParseMode("html");
                } catch (Exception ex) {
                }
                try {
                    PARENT.sendMessage(m);
                } catch (TelegramApiException ex) {
                    error = ant.error(ex);
                    Logger.getLogger(BotTelegram.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (!error.equals("")) {
                texto_log += "Texto:" + texto + "\n" + error + "\n";
            }
            texto_log += "Fin - " + (new MyOwnCalendar()).toString();
            //System.out.println(PARENT.getUltima_parada());

            if (PARENT.isFullLog() || !error.equals("")) {
                System.out.println(Log.Log(texto_log));
            }
        }

        return true;
    }

}
