/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos;

import utils.time.MyOwnCalendar;

/**
 *
 * @author eryalus
 */
public class Contacto {

    private final String txt;
    private final Long time;

    public String getTxt() {
        return txt;
    }

    public Long getTime() {
        return time;
    }

    public String getFormattedTime() {
        MyOwnCalendar calendar = new MyOwnCalendar();
        calendar.setTimeInMillis(time);
        return calendar.getTimeForFilename().replace("_", " ");
    }

    public Contacto(String txt, Long time) {
        this.txt = txt;
        this.time = time;
    }
}
