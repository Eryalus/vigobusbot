/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos;

import java.util.ArrayList;
import java.util.Calendar;

/**
 *
 * @author eryalus
 */
public class InfoParada {

    private final Integer MINUTOS = 1;
    private final String texto;
    private final Integer numero;
    private final Long time;
    private ArrayList<String> llegada_buses = null;

    public InfoParada(Integer numero, String texto) {
        this.texto = texto;
        this.numero = numero;
        this.time = Calendar.getInstance().getTimeInMillis() + MINUTOS * 60 * 1000;
    }

    public InfoParada(Integer numero, String texto, Long time) {
        this.texto = texto;
        this.numero = numero;
        this.time = time;
    }

    @Override
    public String toString() {
        String tmp = texto + ";;" + numero + ";;" + time + ";;" + MINUTOS + ";;";
        for (String s : llegada_buses) {
            tmp += s + ";;";
        }
        tmp = tmp.substring(0, tmp.length() - 2);
        return tmp;
    }

    public static InfoParada reverseToString(String toString) {
        if (toString == null) {
            return null;
        }
        String[] partes = toString.split(";;");
        String texto = partes[0];
        int numero = Integer.parseInt(partes[1]);
        Long time = Long.parseLong(partes[2]);
        ArrayList<String> al = new ArrayList<>();
        for (int i = 3; i < partes.length; i++) {
            al.add(partes[i]);
        }
        InfoParada tmp = new InfoParada(numero, texto, time);
        tmp.setLlegadas(al);
        return tmp;
    }

    public InfoParada setLlegadas(ArrayList<String> llegadas) {
        llegada_buses = llegadas;
        return this;
    }

    public ArrayList<String> getLlegadas() {
        return llegada_buses;
    }

    public Long getTime() {
        return time;
    }

    public Integer getNumeroI() {
        return numero;
    }

    public String getNumeroS() {
        return Integer.toString(numero);
    }

    public String getText() {
        return texto;
    }
}
