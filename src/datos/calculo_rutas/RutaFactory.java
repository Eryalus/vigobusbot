/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos.calculo_rutas;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author eryalus
 */
public class RutaFactory {

    public static ArrayList<DetailedTramo> getTramos(RutaFactoryData data) {
        return getTramos(data.getHora(), data.getMinuto(), data.getDia(), data.getMes(), data.getYear(), data.getLatitudOrigen(), data.getLongitudOrigen(), data.getLatitudDestino(), data.getLongitudDestino());
    }

    public static ArrayList<DetailedTramo> getTramos(String hora, String minuto, String dia, String mes, String year, String lat_o, String lon_o, String lat_d, String lon_d) {
        String last_parada = null;
        DetailedTramo last_tramo = null;
        ArrayList<DetailedTramo> tramos = new ArrayList<>();
        try {
            String file = new Random().nextLong()+hora+"_"+minuto+"_bot.txt";
            URL u = new URL("http://sira.intecoingenieria.com/calculounica.aspx?Directo=true&DireccionO=Origen&DireccionD=Destino&Hora=" + hora + "%3a" + minuto + "&Dia=" + dia + "%2f" + mes + "%2f" + year + "&CoordenadasOrigen=" + lat_o + "%2c" + lon_o + "&Coordenadasdestino=" + lat_d + "%2c" + lon_d + "&NombreArchivo=" + file);
            BufferedReader br = new BufferedReader(new InputStreamReader(u.openStream(), "ISO-8859-1"));
            u = new URL("http://sira.intecoingenieria.com/gmaps/" + file);
            br = new BufferedReader(new InputStreamReader(u.openStream(), "ISO-8859-1"));
            String line;
            while ((line = br.readLine()) != null) {
                if (line.trim().contains("<description>")) {//</table></td><td><img src=\"/WebResource.axd?d=boYDEsO")) {
                    String type = line.split("<description>")[1].split("<")[0];
                    if (type.equals("Origen")) {
                        continue;
                    }
                    if (!type.startsWith("Nº")) {
                        if (last_parada != null && last_tramo != null) {
                            last_tramo.setParada_fin(last_parada);
                            tramos.add(last_tramo);
                            last_tramo = null;
                        }
                        DetailedTramo t = new DetailedTramo();
                        t.setTipo(type);
                        while ((line = br.readLine()) != null) {
                            if (line.trim().contains("<starttime>")) {
                                t.setInicio(line.split("<starttime>")[1].split("<")[0]);
                            } else if (line.trim().contains("<endtime>")) {
                                t.setFin(line.split("<endtime>")[1].split("<")[0]);
                            } else if (line.contains("<from>")) {
                                t.setDesde(line.split("<from>")[1].split("<")[0]);
                            } else if (line.contains("<to>")) {
                                t.setHasta(line.split("<to>")[1].split("<")[0]);
                            } else if (line.contains("<distance>")) {
                                t.setDistancia(line.split("<distance>")[1].split("<")[0]);
                                break;
                            }
                        }
                        switch (type) {
                            case "Andar":
                                tramos.add(t);
                                last_tramo = null;
                                break;
                            case "Destino":
                                break;
                            default:
                                last_tramo = t;
                                break;
                        }
                    } else {
                        last_parada = line.split(":")[1].split("\\.")[0];
                        if (line.contains("Hora de paso")) {
                            if (last_tramo != null) {
                                last_tramo.setParada_inicio(last_parada);
                            }
                        }
                    }
                }
            }
        } catch (MalformedURLException ex) {
        } catch (IOException ex) {
        }
        return tramos;
    }

    /**
     * deprecated
     *
     * @param data
     * @return
     */
    public static Ruta getRuta(RutaFactoryData data) {
        Ruta ruta = new Ruta();
        try {
            URL u = new URL("http://sira.intecoingenieria.com/?Directo=true&DireccionO=origen&DireccionD=destino&Hora=" + data.getHora() + "%3A" + data.getMinuto() + "&Dia=" + data.getDia() + "%2F" + data.getMes() + "%2F" + data.getYear() + "&CoordenadasOrigen=" + data.getLatitudOrigen() + "," + data.getLongitudOrigen() + "&Coordenadasdestino=" + data.getLatitudDestino() + "," + data.getLongitudDestino());
            BufferedReader br = new BufferedReader(new InputStreamReader(u.openStream(), "ISO-8859-1"));
            String line;
            ArrayList<Tramo> tramos = new ArrayList<>();
            Tramo t = new Tramo();
            while ((line = br.readLine()) != null) {
                //  System.out.println(line);
                if (line.trim().contains("</table></td><td><img src=\"/WebResource.axd?d=boYDEsO")) {
                    if (line.contains("Hora de Salida")) {
                        ruta.setH_inicio(getParam(line));
                    } else if (line.contains("Hora de Llegada:")) {
                        ruta.setH_fin(getParam(line));
                    } else if (line.contains("Distancia Recorrida:")) {
                        ruta.setDistancia(getParam(line));
                    } else if (line.contains("Transbordos:")) {
                        ruta.setTransbordos(getParam(line));
                    } else if (line.contains("Inicio de tramo:")) {
                        t.setInicio(getParam(line));
                    } else if (line.contains("Final de tramo:")) {
                        t.setFin(getParam(line));
                        tramos.add(t);
                        t = new Tramo();
                    } else if (line.contains("Distancia andando de la")) {
                        ruta.setDistancia_andando_ultima_parada(getParam(line));
                    }
                }
            }
            ruta.setTramos(tramos);
        } catch (MalformedURLException ex) {
            Logger.getLogger(RutaFactory.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(RutaFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ruta;
    }

    private static String getParam(String txt) {
        if (txt.split("\"texto_gris_oscurob\">").length >= 2) {
            return txt.split("\"texto_gris_oscurob\">")[1].split("<")[0];
        } else {
            return null;
        }
    }
}
