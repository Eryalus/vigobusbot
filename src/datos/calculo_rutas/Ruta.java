/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos.calculo_rutas;

import java.util.ArrayList;

/**
 *
 * @author eryalus
 */
public class Ruta {

    private String h_inicio, h_fin, distancia, transbordos, distancia_andando_ultima_parada;

    public String getDistancia_andando_ultima_parada() {
        return distancia_andando_ultima_parada;
    }

    public void setDistancia_andando_ultima_parada(String distancia_andando_ultima_parada) {
        this.distancia_andando_ultima_parada = distancia_andando_ultima_parada;
    }

    public String getH_inicio() {
        return h_inicio;
    }

    public void setH_inicio(String h_inicio) {
        this.h_inicio = h_inicio;
    }

    public String getH_fin() {
        return h_fin;
    }

    public void setH_fin(String h_fin) {
        this.h_fin = h_fin;
    }

    public String getDistancia() {
        return distancia;
    }

    public void setDistancia(String distancia) {
        this.distancia = distancia;
    }

    public String getTransbordos() {
        return transbordos;
    }

    public void setTransbordos(String transbordos) {
        this.transbordos = transbordos;
    }

    public ArrayList<Tramo> getTramos() {
        return tramos;
    }

    public void setTramos(ArrayList<Tramo> tramos) {
        this.tramos = tramos;
    }
    private ArrayList<Tramo> tramos = new ArrayList<>();
}
