/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos.calculo_rutas;

/**
 *
 * @author eryalus
 */
public class DetailedTramo {

    private String Desde, Hasta, Inicio, Fin, Distancia, Tipo, Parada_inicio, Parada_fin;

    @Override
    public String toString() {
        String txt = "";
        if (Tipo.equals("Andar")) {
            txt += "<b>Andando</b>";
        } else {
            txt += "<b>Línea:</b> " + Tipo;
        }
        txt += "\n<b>Desde:</b> " + Desde + "\n<b>Hasta:</b> " + Hasta + "\n<b>Salida:</b>" + Inicio + "\n<b>Llegada:</b> " + Fin + "\n<b>Distancia:</b> " + Distancia+"m";
        if (Parada_fin != null && Parada_inicio != null) {
            txt += "\n<b>Nº parada origen:</b> /" + Parada_inicio.trim() + "L\n<b>Nº parada destino:</b> /" + Parada_fin.trim() + "L";
        }
        return txt;
    }

    public String getParada_inicio() {
        return Parada_inicio;
    }

    public void setParada_inicio(String Parada_inicio) {
        this.Parada_inicio = Parada_inicio;
    }

    public String getParada_fin() {
        return Parada_fin;
    }

    public void setParada_fin(String Parada_fin) {
        this.Parada_fin = Parada_fin;
    }

    public String getDesde() {
        return Desde;
    }

    public void setDesde(String Desde) {
        this.Desde = Desde;
    }

    public String getHasta() {
        return Hasta;
    }

    public void setHasta(String Hasta) {
        this.Hasta = Hasta;
    }

    public String getInicio() {
        return Inicio;
    }

    public void setInicio(String Inicio) {
        this.Inicio = Inicio;
    }

    public String getFin() {
        return Fin;
    }

    public void setFin(String Fin) {
        this.Fin = Fin;
    }

    public String getDistancia() {
        return Distancia;
    }

    public void setDistancia(String Distancia) {
        this.Distancia = Distancia;
    }

    public String getTipo() {
        return Tipo;
    }

    public void setTipo(String Tipo) {
        this.Tipo = Tipo;
    }
}
