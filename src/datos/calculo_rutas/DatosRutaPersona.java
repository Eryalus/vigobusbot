/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos.calculo_rutas;

/**
 *
 * @author eryalus
 */
public class DatosRutaPersona {

    String tiempo, ubicacion_origen, ubicacion_destino;

    public RutaFactoryData getFactoryData() {
        RutaFactoryData data = new RutaFactoryData();
        String H, M, D, MES, A;
        H = tiempo.substring(0, 2);
        M = tiempo.substring(3, 5);
        D = tiempo.substring(6, 8);
        MES = tiempo.substring(9, 11);
        A = tiempo.substring(12);
        String lat_o = ubicacion_origen.split(",")[0];
        String lon_o = ubicacion_origen.split(",")[1];
        String lat_d = ubicacion_destino.split(",")[0];
        String lon_d = ubicacion_destino.split(",")[1];
        data.setDia(D);
        data.setHora(H);
        data.setMes(MES);
        data.setMinuto(M);
        data.setYear(A);
        data.setLatitudOrigen(lat_o);
        data.setLongitudOrigen(lon_o);
        data.setLatitudDestino(lat_d);
        data.setLongitudDestino(lon_d);
        return data;
    }

    public String getTiempo() {
        return tiempo;
    }

    public void setTiempo(String tiempo) {
        this.tiempo = tiempo;
    }

    public String getUbicacion_origen() {
        return ubicacion_origen;
    }

    public void setUbicacion_origen(String ubicacion_origen) {
        this.ubicacion_origen = ubicacion_origen;
    }

    public String getUbicacion_destino() {
        return ubicacion_destino;
    }

    public void setUbicacion_destino(String ubicacion_destino) {
        this.ubicacion_destino = ubicacion_destino;
    }
}
