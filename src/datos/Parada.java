/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos;

/**
 *
 * @author eryalus
 */
public class Parada {

    private final String altura, nombre;
    private final double lat, lon;
    private final int num;

    @Override
    public String toString() {
        return "Nº " + num + ", " + nombre;
    }

    public String getParWithSlash() {

        return "Nº /" + num + ", " + nombre;
    }

    public Parada(int numero, double latitud, double longitud, String altura, String nombre) {
        this.altura = altura;
        this.lat = latitud;
        this.lon = longitud;
        this.num = numero;
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public int getNum() {
        return num;
    }

    public double getLatitud() {
        return lat;
    }

    public double getLongitud() {
        return lon;
    }

    public String getAltitud() {
        return altura;
    }
}
