/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos;

import java.util.HashMap;

/**
 *
 * @author eryalus
 */
public class Persona {

    String user, name, apellidos;
    Long id_chat;
    HashMap<Integer,ParadaFavorita> paradas = new HashMap<>();

    @Override
    public String toString() {
        return "Chat: " + id_chat + " -- Usuario: @" + user + " -> " + name + " " + apellidos;
    }

    public Persona(Long id, String user, String name, String apellidos, HashMap<Integer,ParadaFavorita>  listado) {
        this.user = user;
        this.name = name;
        this.apellidos = apellidos;
        id_chat = id;
        paradas = listado;
    }

    public Long getID() {
        return id_chat;
    }

    public String getUserName() {
        return user;
    }

    public String getNombre() {
        return name;
    }

    public String getApellidos() {
        return apellidos;
    }

    public HashMap<Integer,ParadaFavorita>  getFavs() {
        return paradas;
    }

    public boolean delFav(Integer parada) {
        if(paradas.remove(parada)==null){
            return false;
        }else{
            return true;
        }
    }

    public boolean isFav(Integer parada) {
        return paradas.containsKey(parada);
    }

    public boolean addFav(ParadaFavorita parada) {
        if (!paradas.containsKey(parada.getFavoritaI())){
            paradas.put(parada.getFavoritaI(), parada);
            return true;
        } else {
            return false;
        }
    }
}
