/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos;

/**
 *
 * @author eryalus
 */
public class ParadaFavorita {

    private final Integer numero;
    private final String alias;

    public ParadaFavorita(Integer numero, String alias) {
        this.numero = numero;
        this.alias = alias;

    }

    public Integer getFavoritaI() {
        return numero;
    }

    public String getFavoritaS() {
        return numero.toString();
    }

    public String getAlias() {
        return alias;
    }
}
