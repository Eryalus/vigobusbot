/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos;

/**
 *
 * @author eryalus
 */
public class Noticia {

    private String url, title, date, description;
    private final String BASE = "http://www.vitrasa.es/php/";
    public static String URL = "http://www.vitrasa.es/php/index.php?pag=empresa/noticias";

    public String getUrl() {
        return BASE + url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
