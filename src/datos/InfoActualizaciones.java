/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos;

import java.util.Objects;

/**
 *
 * @author eryalus
 */
public class InfoActualizaciones implements Comparable {

    private Integer tiempo_rest, sort = 1;
    private final Integer parada, tiempo;
    private final String linea;
    private Long id = null;

    public InfoActualizaciones(Integer parada, String linea, Integer tiempo) {
        this.parada = parada;
        this.tiempo = tiempo;
        this.linea = linea;
        tiempo_rest = -1;
    }

    public InfoActualizaciones setID(Long id) {
        this.id = id;
        return this;
    }

    public Long getID() {
        return id;
    }

    public InfoActualizaciones setSort(Integer sort) {
        this.sort = sort;
        return this;
    }

    @Override
    public String toString() {
        return "P: " + parada + ", L: \"" + linea + "\", T: " + tiempo;
    }

    public Integer getRestante() {
        return tiempo_rest;
    }

    public void setRestante(Integer tiempo) {
        tiempo_rest = tiempo;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 19 * hash + Objects.hashCode(this.parada);
        hash = 19 * hash + Objects.hashCode(this.tiempo);
        hash = 19 * hash + Objects.hashCode(this.linea);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final InfoActualizaciones other = (InfoActualizaciones) obj;
        if (!this.linea.equals(other.linea)) {
            return false;
        }
        if (!this.parada.equals(other.parada)) {
            return false;
        }
        return this.tiempo.equals(other.tiempo);
    }

    public String getLinea() {
        return linea;
    }

    public Integer getParada() {
        return parada;
    }

    public Integer getTiempo() {
        return tiempo;
    }

    @Override
    public int compareTo(Object o) {
        if (o instanceof InfoActualizaciones) {
            if (sort == 1) { //parada
                return Integer.compare(parada, ((InfoActualizaciones) o).parada);
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }
}
