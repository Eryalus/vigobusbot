/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package maps;

import java.util.ArrayList;
import java.util.concurrent.Semaphore;

import org.telegram.telegrambots.api.methods.send.SendPhoto;
import org.telegram.telegrambots.api.objects.Location;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import principal.BotTelegram;

public class googleMaps extends Thread {

    private Thread hilo = null;
    private final static String direccion = "https://maps.googleapis.com/maps/api/staticmap?size=500x500";
    private int contador = 0;
    private final static String marcador = "&markers=color:red%7Clabel:";
    private final static String ubicacion = "&markers=color:blue%7C";
    private final ArrayList<datos.Parada> paradas;
    private final Location loc;
    private final BotTelegram PARENT;
    private final SendPhoto foto = new SendPhoto();
    private String URL = new String();
    private final Semaphore sem;

    public googleMaps(ArrayList<datos.Parada> paradas, Location loc, BotTelegram parent, Long id_chat, Semaphore sem) {
        this.paradas = paradas;
        this.loc = loc;
        PARENT = parent;
        foto.setChatId(id_chat);
        this.sem = sem;
    }

    @Override
    public void start() {
        if (hilo == null) {
            hilo = new Thread(this, "Hilo maps");
            hilo.start();
        }
    }

    @Override
    public void run() {
        URL = direccion + ubicacion + loc.getLatitude() + "," + loc.getLongitude();
        for (datos.Parada lista : paradas) {
            URL = URL + marcador + contador + "%7C" + lista.getLatitud() + "," + lista.getLongitud();
            contador++;
        }
        try {
            foto.setPhoto(URL);
            PARENT.sendPhoto(foto);
        } catch (TelegramApiException e) {
        }
        sem.release();
    }
}
