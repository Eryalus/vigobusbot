/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package maps;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author eryalus
 */
public class db {

    /**
     * Retorna un array con las paradas las cercanas que cumplan los siguientes
     * datos
     *
     * @param latitud Latitud
     * @param longitud Longitud
     * @param distancia Distancia máxima parada
     * @param max Numero máximo de paradas
     * @param conn Conexión con la base de datos
     * @return Lista de paradas
     */
    public static ArrayList<Integer> getParadasGeo(Float latitud, Float longitud, Integer distancia, Integer max, Connection conn) {
        String lat,lon;
        lat = String.valueOf(latitud).replace(",", ".");
        lon = String.valueOf(longitud).replace(",", ".");
        ArrayList<Integer> al = new ArrayList<>();
        String texto = "select id,3956 * 2 * ASIN(SQRT(POWER(SIN((" + lat
                + "-latitud) * pi()/180 / 2), 2) +COS(" + lon
                + " * pi()/180) *COS(latitud * pi()/180) *POWER(SIN((" + lon
                + " -longitud) * pi()/180 / 2), 2) )) as distance from paradas having distance < " + distancia
                + " order by distance limit " + max + ";";
        try {
            Statement st = conn.createStatement();
            ResultSet result = st.executeQuery(texto);
            while (result.next()) {
                al.add(result.getInt("id"));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
        return al;
    }
}
