/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estados;

import principal.BotTelegram;
import java.util.ArrayList;
import org.telegram.telegrambots.api.methods.send.SendMessage;

/**
 *
 * @author eryalus
 */
public abstract class Estado {

    protected final BotTelegram PARENT;
    public static final int ESTADO_BUSCAR=8,ESTADO_CONTACTA=9,ESTADO_EVNIAR_ADMIN=6,ESTADO_GENERAL=0,ESTADO_GENERAL_ADMIN=0,ESTADO_GEO=7,ESTADO_PARADA_ADD=3,ESTADO_PARADA_ADD_NOMBRE=5,ESTADO_PARADA_BORRAR=2,ESTADO_PONER_NOMBRE=4,ESTADO_TIEMPO_PARADA=1,ESTADO_GET_TIME_CALCULO_RUTA=10,ESTADO_UBICACION_ORIGEN=11,ESTADO_UBICACION_DESTINO=12;
    public Estado(BotTelegram bot) {
        PARENT = bot;
    }

    public abstract ArrayList<SendMessage> response(ArrayList<SendMessage> ms);
}
