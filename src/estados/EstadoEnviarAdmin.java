/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estados;

import principal.BotTelegram;
import principal.RespuestaConsulta;
import static comandos.MessageEditor.removeTeclado;
import comandos.administrador.Send;
import java.util.ArrayList;
import org.telegram.telegrambots.api.methods.send.SendMessage;

/**
 *
 * @author eryalus
 */
public class EstadoEnviarAdmin extends Estado {

    private final String text, txt;
    private final Long id;
    private final RespuestaConsulta ant;

    public EstadoEnviarAdmin(String text, Long id, RespuestaConsulta ant, BotTelegram bot) {
        super(bot);
        this.text = text;
        this.txt = text.toLowerCase();
        this.id = id;
        this.ant = ant;
    }

    @Override
    public ArrayList<SendMessage> response(ArrayList<SendMessage> ms) {
        SendMessage m = new SendMessage();
        switch (txt) {
            case "salir":
                m.setText("Ha vuelto al inicio");
                PARENT.putEstado(id, Estado.ESTADO_GENERAL);
                m = removeTeclado(m);
                ms.add(m);
                break;
            default:
                ms = new Send(id, text, PARENT, ant).addMessages(ms);
                break;
        }
        return ms;
    }

}
