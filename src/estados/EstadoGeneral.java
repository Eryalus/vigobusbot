/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estados;

import datos.InfoParada;
import datos.Persona;
import principal.BotTelegram;
import principal.RespuestaConsulta;
import static comandos.MessageEditor.removeTeclado;
import static comandos.MessageEditor.setTecladoSalir;
import static comandos.MessageEditor.setTecladoTiempoBusqueda;
import comandos.usuario.AddFav;
import comandos.usuario.Contacta;
import comandos.usuario.DeleteEverything;
import comandos.usuario.Donar;
import comandos.usuario.GetHora;
import comandos.usuario.GetPlano;
import comandos.usuario.GetRuta;
import comandos.usuario.GetSeguimientos;
import comandos.usuario.GetTarifas;
import comandos.usuario.Help;
import comandos.usuario.InitCalculoRuta;
import comandos.usuario.ListarRutas;
import comandos.usuario.MyInfo;
import comandos.usuario.Noticias;
import comandos.usuario.SendLocationParada;
import comandos.usuario.Start;
import java.util.ArrayList;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;

/**
 *
 * @author eryalus
 */
public class EstadoGeneral extends Estado {

    private Persona per;
    private Long id;
    private String texto;
    private RespuestaConsulta ant;
    private Integer parada = null;
    private ArrayList<String> lista = new ArrayList<>();
    private String texto_to_return = "";

    public Integer getParada() {
        return parada;
    }

    public ArrayList<String> getLista() {
        return lista;
    }

    public String getTexto() {
        return texto_to_return;
    }

    public EstadoGeneral(Persona per, Long id, String texto, RespuestaConsulta ant, BotTelegram bot) {
        super(bot);
        this.per = per;
        this.id = id;
        this.texto = texto;
        this.ant = ant;
    }

    @Override
    public ArrayList<SendMessage> response(ArrayList<SendMessage> ms) {
        Integer parada = null;
        String txt = texto.toLowerCase();
        if (txt.equals("/start")) {
            ArrayList<SendMessage> maux = new ArrayList<>();
            ms = new Start().addMessages(ms);
        } else if (txt.startsWith("/add")) {
            SendMessage m = new SendMessage();
            ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();
            ArrayList<KeyboardRow> al = new ArrayList<>();
            KeyboardRow r = new KeyboardRow();
            r.add("Cancelar");
            al.add(r);
            r = new KeyboardRow();
            r.add("Salir");
            al.add(r);
            keyboardMarkup.setKeyboard(al);
            m.setReplyMarkup(keyboardMarkup);
            m.setText("Escriba el numero de parada a añadir");
            ArrayList<SendMessage> maux = new ArrayList<>();
            maux.add(m);
            ms = maux;
            PARENT.putEstado(id, Estado.ESTADO_PARADA_ADD);
        } else if (txt.startsWith("/fav")) {
            ms = new comandos.usuario.GetFav(id, dataBase.Basics.cargarParadas(PARENT.getConnection(), false), PARENT.getConnection(), ant).addMessages(ms);//Utils.Comandos.getFav(id, PARENT.paradas, PARENT.getConnection(), this);
        } else if (txt.startsWith("/del")) {
            SendMessage m = new SendMessage();
            ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();
            ArrayList<KeyboardRow> al = new ArrayList<>();
            KeyboardRow r = new KeyboardRow();
            r.add("Todas");
            al.add(r);
            r = new KeyboardRow();
            r.add("Cancelar");
            al.add(r);
            r = new KeyboardRow();
            r.add("Salir");
            al.add(r);
            keyboardMarkup.setKeyboard(al);
            m.setReplyMarkup(keyboardMarkup);
            m.setText("Escriba el numero de parada a eliminar o pulse \"Todas\" para borrar todas");
            ArrayList<SendMessage> maux = new ArrayList<>();
            maux.add(m);
            ms = maux;
            PARENT.putEstado(id, 2);
        } else if (txt.equals("/help")) {
            ms = new Help().addMessages(ms);
        } else if (txt.equals("/seguimientos")) {
            ms = new GetSeguimientos(id, PARENT).addMessages(ms);
        } else if (txt.equals("/plano")) {
            ms = new GetPlano(id, PARENT).addMessages(ms);
        } else if (txt.equals("/geo")) {
            SendMessage m = new SendMessage();
            m.setText("Envie su ubicacion para ver las paradas más cercanas.\n\n<i>Actualmente ya no es necesario escribir este comando antes de enviar la ubicación</i>");
            m = setTecladoSalir(m);
            ArrayList<SendMessage> maux = new ArrayList<>();
            maux.add(m);
            ms = maux;
            PARENT.putEstado(id, Estado.ESTADO_GEO);

        } else if (txt.equals("/buscar")) {
            SendMessage m = new SendMessage();
            m.setText("Ingrese el texto a buscar");
            m = setTecladoSalir(m);
            ms.add(m);
            PARENT.putEstado(id, Estado.ESTADO_BUSCAR);

        } else if (txt.equals("/contacta")) {
            ms = new Contacta().addMessages(ms);
            PARENT.putEstado(id, Estado.ESTADO_CONTACTA);
        } else if (txt.equals("/tarifas")) {
            ms = new GetTarifas().addMessages(ms);
        } else if (txt.equalsIgnoreCase("/listarLineas")) {
            ms = new ListarRutas().addMessages(ms);
        } else if (txt.equalsIgnoreCase("/miinfo")) {
            ms = new MyInfo(id, PARENT.getConnection()).addMessages(ms);
        } else if (txt.equalsIgnoreCase("/borrartodoslosdatos")) {
            ms = new DeleteEverything(id, PARENT).addMessages(ms);
        } else if (txt.equalsIgnoreCase("/noticias")) {
            ms = new Noticias().addMessages(ms);
        } else if (txt.equalsIgnoreCase("/donar")) {
            ms = new Donar().addMessages(ms);
        }else if(txt.equalsIgnoreCase("/calculoruta")){
            ms = new InitCalculoRuta().addMessages(ms);
            PARENT.putEstado(id, Estado.ESTADO_GET_TIME_CALCULO_RUTA);
        } else if (texto.startsWith("/") && txt.endsWith("l")) {
            String numero = texto.substring(1, texto.length() - 1); //quito la barra (/) y la L del final
            ms = new SendLocationParada(id, numero, PARENT).addMessages(ms);
            GetHora gh = new GetHora("/" + numero, per, dataBase.Basics.cargarParadas(PARENT.getConnection(), false), PARENT.getConnection(), ant);
            ms = gh.addMessages(ms);
            parada = gh.getNumero();
            lista = gh.getBuses();
            texto_to_return = ms.get(ms.size() - 1).getText();

        } else if (texto.startsWith("/") && txt.endsWith("r")) {
            String code = texto.substring(1, texto.length() - 1); //quito la barra (/) y la R del final
            ms = new GetRuta(id, code, PARENT).addMessages(ms);
        } else if (texto.startsWith("/")) {
            GetHora gh = new GetHora(texto, per, dataBase.Basics.cargarParadas(PARENT.getConnection(), false), PARENT.getConnection(), ant);
            ms = gh.addMessages(ms);
            parada = gh.getNumero();
            lista = gh.getBuses();
            texto_to_return = ms.get(ms.size() - 1).getText();
        } else if (txt.equals("cancelar") | texto.toLowerCase().equals("salir")) {
            SendMessage m = removeTeclado(new SendMessage());
            m.setText("Ha vuelto al inicio");
            ms.add(m);
        } else if (texto.equals("Añadir a favoritos")) {
            Integer n_parada = PARENT.getUltima_parada(id).getNumeroI();
            if (n_parada != null) {
                AddFav af = new AddFav(n_parada, null, id, dataBase.Basics.cargarParadas(PARENT.getConnection(), false), PARENT.getConnection(), ant);
                ms = af.addMessages(ms);
            } else {
                SendMessage m = new SendMessage();
                m.setText("No se ha podido añadir a favoritos");
                ArrayList<SendMessage> maux = new ArrayList<>();
                maux.add(m);
                ms = maux;
            }
        } else {
            InfoParada info = PARENT.getUltima_parada(id);
            if (info == null) {
                SendMessage m = new SendMessage();
                m.setText("Busque una parada: /[numero parada]");
                ArrayList<SendMessage> maux = new ArrayList<>();
                maux.add(m);
                ms = maux;
            } else {
                SendMessage m = new SendMessage();
                ArrayList<String> llegadas = info.getLlegadas();
                boolean contenido = false;
                for (String linea : llegadas) {
                    //System.out.println(linea + "  " + texto);
                    if (linea.toLowerCase().startsWith(texto.toLowerCase())) {
                        contenido = true;
                        break;
                    }
                }
                if (texto.equals("")) { //no hay texto
                    contenido = false;
                }
                if (contenido) {
                    m.setText("Minutos restantes para ser avisado\n<i>Minimo 3 minutos</i>");
                    m = setTecladoTiempoBusqueda(m);
                    ArrayList<SendMessage> maux = new ArrayList<>();
                    maux.add(m);
                    ms = maux;
                    PARENT.putLineas_solicitadas(id, texto);
                    PARENT.putEstado(id, Estado.ESTADO_TIEMPO_PARADA);
                } else {
                    m.setText("No ha introducido una linea valida para el seguimiento");
                    ArrayList<SendMessage> maux = new ArrayList<>();
                    maux.add(m);
                    ms = maux;
                }
            }

        }
        this.parada = parada;
        return ms;
    }

}
