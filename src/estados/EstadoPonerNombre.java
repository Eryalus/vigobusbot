/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estados;

import principal.BotTelegram;
import static comandos.MessageEditor.removeTeclado;
import java.util.ArrayList;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;

/**
 *
 * @author eryalus
 */
public class EstadoPonerNombre extends Estado {

    private final Long id;
    private final String texto;

    public EstadoPonerNombre(Long id, String texto, BotTelegram bot) {
        super(bot);
        this.id = id;
        this.texto = texto;
    }

    @Override
    public ArrayList<SendMessage> response(ArrayList<SendMessage> ms) {
        SendMessage m = new SendMessage();
        switch (texto.toLowerCase()) {
            case "salir":
                m.setText("Ha vuelto al inicio");
                PARENT.putEstado(id, Estado.ESTADO_GENERAL);
                m = removeTeclado(m);
                break;
            case "cancelar":
                ReplyKeyboardMarkup kb1 = new ReplyKeyboardMarkup();
                ArrayList<KeyboardRow> al1 = new ArrayList<>();
                KeyboardRow r1 = new KeyboardRow();
                r1.add("Cancelar");
                al1.add(r1);
                r1 = new KeyboardRow();
                r1.add("Salir");
                al1.add(r1);
                kb1.setKeyboard(al1);
                m.setReplyMarkup(kb1);
                m.setText("Escriba el numero de parada a añadir");
                PARENT.putEstado(id, Estado.ESTADO_PARADA_ADD);
                break;
            case "poner nombre":
                m.setText("Escriba el nombre que quiere darle a la parada");
                ReplyKeyboardMarkup kb = new ReplyKeyboardMarkup();
                ArrayList<KeyboardRow> al = new ArrayList<>();
                KeyboardRow r = new KeyboardRow();
                r.add("Cancelar");
                al.add(r);
                r = new KeyboardRow();
                r.add("Salir");
                al.add(r);
                kb.setKeyboard(al);
                m.setReplyMarkup(kb);
                PARENT.putEstado(id, Estado.ESTADO_PARADA_ADD_NOMBRE);
                break;
            default:
                m.setText("Escoja una opción por favor");
                break;
        }
        ms.add(m);
        return ms;
    }

}
