/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estados;

import comandos.MessageEditor;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import org.glassfish.grizzly.http.server.util.SimpleDateFormats;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import principal.BotTelegram;
import utils.time.MyOwnCalendar;

/**
 *
 * @author eryalus
 */
public class EstadoGetTimeCalculoRuta extends Estado {

    private final Long chat_id;
    private final String texto;
    private static final String REGEX = "[\\d]{2}[:][\\d]{2}\\s[\\d]{2}[\\/][\\d]{2}[\\/][\\d]{4}";
    private static final String FORMAT_ERROR = "Por favor introduzca la fecha con formato HH:MM DD/MM/AAAA";
    private static final String EXIT_MSG = "Ha vuelto al inicio", UNK_ERROR = "Error inesperado. " + EXIT_MSG;
    private static final String SUCCESS = "Envíe la ubicación de origen";

    public EstadoGetTimeCalculoRuta(Long chat_id, String texto, BotTelegram bot) {
        super(bot);
        this.chat_id = chat_id;
        this.texto = texto;
    }

    @Override
    public ArrayList<SendMessage> response(ArrayList<SendMessage> ms) {
        SendMessage msg = new SendMessage();
        if (texto.equalsIgnoreCase("salir")) {
            msg.setText(EXIT_MSG);
            msg = MessageEditor.removeTeclado(msg);
            PARENT.putEstado(chat_id, ESTADO_GENERAL);
        } else if (texto.toLowerCase().equals("ahora")) {
            MyOwnCalendar c = new MyOwnCalendar();
            String H, M, D, MES, A;
            H = "" + c.getHour(MyOwnCalendar.HOURS_24);
            if (H.length() == 1) {
                H = "0" + H;
            }
            M = "" + c.getMinutes();
            if (M.length() == 1) {
                M = "0" + M;
            }
            D = "" + c.getDay();
            if (D.length() == 1) {
                D = "0" + D;
            }
            MES = "" + c.getMonth();
            if (MES.length() == 1) {
                MES = "0" + MES;
            }
            A = "" + c.getYear();
            while (A.length() < 4) {
                A = "0" + A;
            }
            String date = H + ":" + M + " " + D + "/" + MES + "/" + A;
            if (dataBase.Basics.setTiempo_llegada(chat_id, date, PARENT.getConnection())) {
                msg.setText(SUCCESS);
                msg = MessageEditor.setTecladoSalir(msg);
                PARENT.putEstado(chat_id, ESTADO_UBICACION_ORIGEN);
            } else {
                msg.setText(UNK_ERROR);
                msg = MessageEditor.removeTeclado(msg);
                PARENT.putEstado(chat_id, ESTADO_GENERAL);
            }
        } else if (texto.matches(REGEX)) {
            try {
                Integer H, M, D, MES, A;
                H = Integer.parseInt(texto.substring(0, 2));
                M = Integer.parseInt(texto.substring(3, 5));
                D = Integer.parseInt(texto.substring(6, 8));
                MES = Integer.parseInt(texto.substring(9, 11));
                A = Integer.parseInt(texto.substring(12));
                MyOwnCalendar cal = new MyOwnCalendar();
                cal.set(A, MES - 1, D, H, M);
                String TIME_OF_CAL = cal.getMinutes() + " " + cal.getHour(MyOwnCalendar.HOURS_24) + " " + cal.getDay() + " " + cal.getMonth() + " " + cal.getYear();
                String TIME_WRITTEN = M + " " + H + " " + D + " " + MES + " " + A;
                if (TIME_WRITTEN.equals(TIME_OF_CAL)) {//M == cal.getMinutes() && H == cal.getHour(MyOwnCalendar.HOURS_24) && D == cal.getDay() && MES == cal.getMonth() && A == cal.getYear()) {
                    if (dataBase.Basics.setTiempo_llegada(chat_id, texto, PARENT.getConnection())) {
                        msg.setText(SUCCESS);
                        msg = MessageEditor.setTecladoSalir(msg);
                        PARENT.putEstado(chat_id, ESTADO_UBICACION_ORIGEN);
                    } else {
                        msg.setText(UNK_ERROR);
                        msg = MessageEditor.removeTeclado(msg);
                        PARENT.putEstado(chat_id, ESTADO_GENERAL);
                    }
                } else {
                    msg.setText(FORMAT_ERROR);
                }
            } catch (Exception ex) {
                msg.setText(UNK_ERROR);
                msg = MessageEditor.removeTeclado(msg);
                PARENT.putEstado(chat_id, ESTADO_GENERAL);
            }
        } else {
            msg.setText(FORMAT_ERROR);
        }
        ms.add(msg);
        return ms;
    }

}
