/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estados;

import comandos.MessageEditor;
import datos.calculo_rutas.DatosRutaPersona;
import datos.calculo_rutas.DetailedTramo;
import datos.calculo_rutas.Ruta;
import datos.calculo_rutas.RutaFactory;
import datos.calculo_rutas.Tramo;
import static estados.Estado.ESTADO_GENERAL;
import java.util.ArrayList;
import jdk.nashorn.internal.objects.annotations.Getter;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Location;
import org.telegram.telegrambots.api.objects.Update;
import principal.BotTelegram;

/**
 *
 * @author eryalus
 */
public class EstadoUbicacionDestino extends Estado {

    private final Update UPDATE;
    private static final String EXIT_MSG = "Ha vuelto al inicio", UNK_ERROR = "Error inesperado. " + EXIT_MSG;
    private static final String NO_ROUTE = "No se ha encontrado ruta válida. " + EXIT_MSG;
    private static final String WRONG = "Envíe la ubicacion de destino";
    private final Long chat_id;

    public EstadoUbicacionDestino(Update update, BotTelegram bot) {
        super(bot);
        UPDATE = update;
        chat_id = update.getMessage().getChatId();
    }

    @Override
    public ArrayList<SendMessage> response(ArrayList<SendMessage> ms) {
        SendMessage msg = new SendMessage();
        if (UPDATE.getMessage().hasLocation()) {
            Location location = UPDATE.getMessage().getLocation();
            if (dataBase.Basics.setUbicacion_destino(chat_id, ("" + location.getLatitude()).replace(",", ".") + "," + ("" + location.getLongitude()).replace(",", "."), PARENT.getConnection())) {
                try {
                    DatosRutaPersona datosCalculoRuta = dataBase.Basics.getDatosCalculoRuta(chat_id, PARENT.getConnection());
                    ArrayList<DetailedTramo> tramos = RutaFactory.getTramos(datosCalculoRuta.getFactoryData());
                    if (!tramos.isEmpty()) {
                        String txt = "";
                        for (DetailedTramo t : tramos) {
                            if ((txt.length() + t.toString().length()) > 4000) {
                                SendMessage m = new SendMessage();
                                m.setText(txt);
                                ms.add(m);
                                txt = t.toString();
                            } else {
                                txt += t.toString() + "\n\n";
                            }
                        }
                        if (!txt.isEmpty()) {
                            SendMessage m = new SendMessage();
                            m.setText(txt);
                            m = MessageEditor.removeTeclado(m);
                            ms.add(m);
                        }
                    } else {
                        msg.setText(NO_ROUTE);
                        msg = MessageEditor.removeTeclado(msg);
                        ms.add(msg);
                    }

                    /* 
                    Ruta ruta = RutaFactory.getRuta(datosCalculoRuta.getFactoryData());
                    String txt = ruta.getH_inicio() + " - " + ruta.getH_fin() + " - " + ruta.getDistancia();
                    if ((txt.equals("13:17 - 13:36 - 1824 m") && ruta.getDistancia_andando_ultima_parada().equals("326 m")) || ruta.getH_fin() == null || ruta.getH_inicio() == null) {
                        msg.setText(NO_ROUTE);
                        msg = MessageEditor.removeTeclado(msg);
                        ms.add(msg);
                    } else {
                        txt += "\n" + ruta.getTransbordos() + " transbordos";
                        msg.setText(txt);
                        msg = MessageEditor.removeTeclado(msg);
                        ms.add(msg);
                        txt = "";
                        int cont_tramos = 1;
                        for (Tramo t : ruta.getTramos()) {
                            if ((txt.length() + ("<b>Tramo " + cont_tramos + "</b>\nInicio: " + t.getInicio() + "\nFin: " + t.getFin() + "\n").length()) > 4000) {
                                SendMessage m = new SendMessage();
                                m.setText(txt);
                                ms.add(m);
                                txt = "<b>Tramo " + cont_tramos++ + "</b>\nInicio: " + t.getInicio() + "\nFin: " + t.getFin() + "\n";
                            } else {
                                txt += "<b>Tramo " + cont_tramos++ + "</b>\nInicio: " + t.getInicio() + "\nFin: " + t.getFin() + "\n";
                            }
                        }
                        if (!txt.isEmpty()) {
                            String fin = "<b>Tramo " + cont_tramos + "</b>\nDistancia andando hasta el destino: " + ruta.getDistancia_andando_ultima_parada();
                            if (txt.length() + fin.length() > 4000) {
                                SendMessage m = new SendMessage();
                                m.setText(txt);
                                ms.add(m);
                                txt = fin;
                            } else {
                                txt += fin;
                            }
                            SendMessage m = new SendMessage();
                            m.setText(txt);
                            ms.add(m);
                        }
                    }
                     */
                } catch (Exception ex) {
                    msg.setText(UNK_ERROR);
                    msg = MessageEditor.removeTeclado(msg);
                    PARENT.putEstado(chat_id, ESTADO_GENERAL);
                    msg = MessageEditor.removeTeclado(msg);
                    ms.add(msg);
                }
            } else {
                msg.setText(UNK_ERROR);
                msg = MessageEditor.removeTeclado(msg);
                ms.add(msg);
            }
            PARENT.putEstado(chat_id, ESTADO_GENERAL);
        } else {
            try {
                if (UPDATE.getMessage().hasText()) {
                    if (UPDATE.getMessage().getText().equalsIgnoreCase("salir")) {
                        msg.setText(EXIT_MSG);
                        msg = MessageEditor.removeTeclado(msg);
                        PARENT.putEstado(chat_id, ESTADO_GENERAL);
                    } else {
                        msg.setText(WRONG);
                    }
                } else {
                    msg.setText(WRONG);
                }
            } catch (Exception ex) {
                msg.setText(UNK_ERROR);
                msg = MessageEditor.removeTeclado(msg);
                PARENT.putEstado(chat_id, ESTADO_GENERAL);
            }
            ms.add(msg);
        }
        return ms;
    }

}
