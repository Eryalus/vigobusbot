/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estados;

import comandos.MessageEditor;
import static estados.Estado.ESTADO_GENERAL;
import java.util.ArrayList;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Location;
import org.telegram.telegrambots.api.objects.Update;
import principal.BotTelegram;

/**
 *
 * @author eryalus
 */
public class EstadoUbicacionOrigen extends Estado {

    private final Update UPDATE;
    private static final String EXIT_MSG = "Ha vuelto al inicio", UNK_ERROR = "Error inesperado. " + EXIT_MSG;
    private static final String SUCCESS = "Envíe la ubicación de destino";
    private static final String WRONG = "Envíe la ubicacion de origen";
    private final Long chat_id;

    public EstadoUbicacionOrigen(Update update, BotTelegram bot) {
        super(bot);
        UPDATE = update;
        chat_id = update.getMessage().getChatId();
    }

    @Override
    public ArrayList<SendMessage> response(ArrayList<SendMessage> ms) {
        SendMessage msg = new SendMessage();
        if (UPDATE.getMessage().hasLocation()) {
            Location location = UPDATE.getMessage().getLocation();
            if (dataBase.Basics.setUbicacion_origen(chat_id, ("" + location.getLatitude()).replace(",", ".") + "," + ("" + location.getLongitude()).replace(",", "."), PARENT.getConnection())) {
                msg.setText(SUCCESS);
                PARENT.putEstado(chat_id, ESTADO_UBICACION_DESTINO);
            } else {
                msg.setText(UNK_ERROR);
                msg = MessageEditor.removeTeclado(msg);
                PARENT.putEstado(chat_id, ESTADO_GENERAL);
            }

        } else {
            try {
                if (UPDATE.getMessage().hasText()) {
                    if (UPDATE.getMessage().getText().equalsIgnoreCase("salir")) {
                        msg.setText(EXIT_MSG);
                        msg = MessageEditor.removeTeclado(msg);
                        PARENT.putEstado(chat_id, ESTADO_GENERAL);
                    } else {
                        msg.setText(WRONG);
                    }
                } else {
                    msg.setText(WRONG);
                }
            } catch (Exception ex) {
                msg.setText(UNK_ERROR);
                msg = MessageEditor.removeTeclado(msg);
                PARENT.putEstado(chat_id, ESTADO_GENERAL);
            }
        }
        ms.add(msg);
        return ms;
    }

}
