/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estados;

import comandos.MessageEditor;
import dataBase.Basics;
import java.util.ArrayList;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.exceptions.TelegramApiException;
import principal.BotTelegram;

/**
 *
 * @author eryalus
 */
public class EstadoContacta extends Estado {

    private final Long id;
    private final String texto;

    public EstadoContacta(Long id, String texto, BotTelegram bot) {
        super(bot);
        this.id = id;
        this.texto = texto;
    }

    @Override
    public ArrayList<SendMessage> response(ArrayList<SendMessage> ms) {
        dataBase.Basics.addContacta(texto, id, super.PARENT.getConnection());
        SendMessage m = new SendMessage();
        if (texto.toLowerCase().equals("salir")) {
            m.setText("Ha vuelto al inicio.");
            m = MessageEditor.removeTeclado(m);
            ms.add(m);
            PARENT.putEstado(id, Estado.ESTADO_GENERAL);
            return ms;
        }
        boolean error = false;
        m.setText("Contacto: " + id + "\n" + texto);
        ArrayList<Long> ids = Basics.getContact(super.PARENT.getConnection());
        if (ids.isEmpty()) {
            error = true;
        }
        for (Long id_c : ids) {
            m.setChatId(id_c);
            try {
                super.PARENT.sendMessage(m);
            } catch (TelegramApiException ex) {
                error = true;
                break;
            }
        }
        SendMessage mes = new SendMessage();
        if (error) {
            mes.setText("No se ha podido enviar el mensaje. Inténtelo más tarde.\nHa vuelto al inicio.");
        } else {
            mes.setText("Se ha enviado el mensaje a contacto.\nHa vuelto al inicio.");
        }
        ms.add(mes);
        PARENT.putEstado(id, Estado.ESTADO_GENERAL);
        return ms;
    }

}
