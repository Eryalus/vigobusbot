/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estados;

import datos.Parada;
import maps.googleMaps;
import principal.BotTelegram;
import static comandos.MessageEditor.removeTeclado;
import java.util.ArrayList;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Location;
import org.telegram.telegrambots.api.objects.Update;

/**
 *
 * @author eryalus
 */
public class EstadoGeo extends Estado {

    private final Update UPDATE;
    private static final Integer MAX_DISTANCIA = 2;
    private static final Integer MAX_PARADAS = 10;

    public EstadoGeo(Update update, BotTelegram bot) {
        super(bot);
        UPDATE = update;
    }

    @Override
    public ArrayList<SendMessage> response(ArrayList<SendMessage> ms) {
        Long id = UPDATE.getMessage().getChatId();
        if (UPDATE.getMessage().hasLocation()) {
            Location loc = UPDATE.getMessage().getLocation();
            ArrayList<Integer> paradas = maps.db.getParadasGeo(loc.getLatitude(), loc.getLongitude(), MAX_DISTANCIA, MAX_PARADAS, PARENT.getConnection());
            if (paradas != null) {
                if (!paradas.isEmpty()) {
                    ArrayList<datos.Parada> info_paradas = new ArrayList<>();
                    String temp = "Estas son las " + paradas.size() + " paradas mas cercanas:\n\n";
                    int conta = 0;
                    for (Integer parada : paradas) {
                        Parada par = dataBase.Basics.cargarParadas(PARENT.getConnection(),false).get(parada);
                        info_paradas.add(par);
                        if ((temp.length() + (par.toString() + " /" + par.getNum() + "L\n").length()) <= 4000) {
                            temp += "<b>" + conta++ + "</b> - " + par + " /" + par.getNum() + "L\n";
                        } else {
                            SendMessage m = new SendMessage();
                            m.setText(temp);
                            ms.add(m);
                            temp = "<b>" + conta++ + "</b> - " + par + " /" + par.getNum() + "L\n";
                        }
                    }
                    Semaphore sem = new Semaphore(0);
                            
                    googleMaps manda_foto = new googleMaps(info_paradas, loc, PARENT, id,sem);
                    manda_foto.start();
                    SendMessage m = new SendMessage();
                    m = removeTeclado(m);
                    m.setText(temp);
                    ms.add(m);
                    PARENT.putEstado(id, Estado.ESTADO_GENERAL);
                    try {
                        sem.acquire();
                    } catch (InterruptedException ex) {
                        Logger.getLogger(EstadoGeo.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    SendMessage m = new SendMessage();
                    m.setText("Está demasiado lejos de las paradas");
                    PARENT.putEstado(id, Estado.ESTADO_GENERAL);
                    m = removeTeclado(m);
                    ms.add(m);
                }
            } else {
                SendMessage m = new SendMessage();
                m.setText("Se ha producido un error y ha vuelto al inicio");
                PARENT.putEstado(id, Estado.ESTADO_GENERAL);
                m = removeTeclado(m);
                ms.add(m);
            }
        } else if (UPDATE.getMessage().getText().toLowerCase().equals("salir")) {
            SendMessage m = new SendMessage();
            m.setText("Ha vuelto al inicio");
            PARENT.putEstado(id, Estado.ESTADO_GENERAL);
            m = removeTeclado(m);
            ms.add(m);
        } else {
            SendMessage m = new SendMessage();
            m.setText("Envie su ubicacion para ver las paradas más cercanas");
            ms.add(m);
        }
        return ms;
    }

}
