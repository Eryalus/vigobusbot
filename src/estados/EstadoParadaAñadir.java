/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estados;

import datos.ParadaFavorita;
import principal.BotTelegram;
import principal.RespuestaConsulta;
import static comandos.MessageEditor.removeTeclado;
import comandos.usuario.AddFav;
import java.util.ArrayList;
import org.telegram.telegrambots.api.methods.send.SendMessage;

/**
 *
 * @author eryalus
 */
public class EstadoParadaAñadir extends Estado {

    private final Long id;
    private final String texto;
    private final RespuestaConsulta ant;

    public EstadoParadaAñadir(Long id, String texto, RespuestaConsulta ant, BotTelegram bot) {
        super(bot);
        this.id = id;
        this.texto = texto;
        this.ant = ant;
    }

    @Override
    public ArrayList<SendMessage> response(ArrayList<SendMessage> ms) {
        SendMessage m = new SendMessage();
        switch (texto.toLowerCase()) {
            case "salir":
                m.setText("Ha vuelto al inicio");
                PARENT.putEstado(id, Estado.ESTADO_GENERAL);
                m = removeTeclado(m);
                break;
            case "cancelar":
                m.setText("Ha salido");
                m = removeTeclado(m);
                PARENT.putEstado(id, Estado.ESTADO_GENERAL);
                break;
            default:
                AddFav af = new AddFav(texto, id, dataBase.Basics.cargarParadas(PARENT.getConnection(), false), PARENT.getConnection(), ant);
                ms = af.addMessages(ms);
                ParadaFavorita fav = af.getFavorita();
                if (fav != null) {
                    PARENT.putEstado(id, Estado.ESTADO_PONER_NOMBRE);
                    PARENT.putUltima_parada_fav(id, fav);
                }
                return ms;
        }
        ms.add(m);
        return ms;
    }

}
