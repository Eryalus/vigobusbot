/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estados;

import datos.Persona;
import principal.BotTelegram;
import principal.RespuestaConsulta;
import static comandos.MessageEditor.removeTeclado;
import comandos.usuario.DeleteFav;
import java.util.ArrayList;
import org.telegram.telegrambots.api.methods.send.SendMessage;

/**
 *
 * @author eryalus
 */
public class EstadoParadaBorrar extends Estado {

    private final Persona per;
    private final Long id;
    private final String texto;
    private final RespuestaConsulta ant;

    public EstadoParadaBorrar(Persona per, Long id, String texto, RespuestaConsulta ant, BotTelegram bot) {
        super(bot);
        this.per = per;
        this.id = id;
        this.texto = texto;
        this.ant = ant;
    }

    @Override
    public ArrayList<SendMessage> response(ArrayList<SendMessage> ms) {
        SendMessage m = new SendMessage();
        switch (texto.toLowerCase()) {
            case "salir":
                m.setText("Ha vuelto al inicio");
                PARENT.putEstado(id, Estado.ESTADO_GENERAL);
                m = removeTeclado(m);
                break;
            case "cancelar":
                m.setText("Ha salido");
                m = removeTeclado(m);
                PARENT.putEstado(id, Estado.ESTADO_GENERAL);
                break;
            default:
                ms = new DeleteFav(texto, id, PARENT.getConnection(), ant).addMessages(ms);
                PARENT.putEstado(id, Estado.ESTADO_GENERAL);
                return ms;
        }
        ms.add(m);
        return ms;
    }

}
