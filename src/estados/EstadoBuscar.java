/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estados;

import dataBase.Basics;
import datos.Parada;
import principal.BotTelegram;
import static comandos.MessageEditor.removeTeclado;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import org.telegram.telegrambots.api.methods.send.SendMessage;

/**
 *
 * @author eryalus
 */
public class EstadoBuscar extends Estado {

    private final Long id;
    private final String texto;

    public EstadoBuscar(Long id, String texto, BotTelegram bot) {
        super(bot);
        this.id = id;
        this.texto = texto;
    }

    @Override
    public ArrayList<SendMessage> response(ArrayList<SendMessage> ms) {
        SendMessage m = new SendMessage();
        m = removeTeclado(m);
        if (texto.equals("Salir")) {
            m.setText("Ha vuelto al inicio");
            PARENT.putEstado(id, Estado.ESTADO_GENERAL);
            ms.add(m);
            return ms;
        }
        HashMap<Integer, Parada> map = Basics.cargarParadas(PARENT.getConnection(), "%" + texto + "%");
        String aux = "<b>Paradas:</b>\n\n";
        Set<Integer> keys = map.keySet();
        for (Integer key : keys) {
            Parada p = map.get(key);
            if ((aux.length() + p.toString().length()) <= 4000) {
                aux += p + " /" + p.getNum() + "L\n";
            } else {
                m.setText(aux);
                ms.add(m);
                m = new SendMessage();
                aux = p + " /" + p.getNum() + "L\n";
            }
        }
        if (aux.equals("<b>Paradas:</b>\n\n")) {
            m.setText("No hay paradas que coincidan con la busqueda");
            ms.add(m);
        } else {
            m.setText(aux);
            ms.add(m);
        }
        PARENT.putEstado(id, 0);
        return ms;
    }

}
