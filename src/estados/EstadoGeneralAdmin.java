/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estados;

import principal.BotTelegram;
import principal.RespuestaConsulta;
import static utils.Basics.getIP;
import static utils.Basics.getIPMessage;
import comandos.administrador.AddOP;
import comandos.administrador.DeleteOP;
import comandos.administrador.Dump;
import comandos.administrador.GetInfo;
import comandos.administrador.GetSeguimientosOP;
import comandos.administrador.GetTimer;
import comandos.administrador.HelpOP;
import comandos.administrador.LogActions;
import comandos.administrador.SendTyped;
import comandos.administrador.SetTimer;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.exceptions.TelegramApiException;

/**
 *
 * @author eryalus
 */
public class EstadoGeneralAdmin extends Estado {

    private final String text;
    private final String txt;
    private final Long id;
    private final RespuestaConsulta ant;

    public EstadoGeneralAdmin(String text, Long id, RespuestaConsulta ant, BotTelegram bot) {
        super(bot);
        this.text = text;
        this.txt = text.toLowerCase();
        this.id = id;
        this.ant = ant;
    }

    @Override
    public ArrayList<SendMessage> response(ArrayList<SendMessage> ms) {
        if (txt.startsWith("/addop")) {
            ms = new AddOP(text, PARENT.getRegcon()).addMessages(ms);
        } else if (txt.startsWith("/delop")) {
            ms = new DeleteOP(text, PARENT.getRegcon()).addMessages(ms);
        } else if (txt.startsWith("/log")) {
            ms = new LogActions(text, id, PARENT, ant).addMessages(ms);
        } else if (txt.startsWith("/getinfo")) {
            ms = new GetInfo(text, PARENT.getRegcon()).addMessages(ms);
        } else if (txt.equals("/help")) {
            ms = new HelpOP().addMessages(ms);
        } else if (txt.startsWith("/dump")) {
            ms = new Dump(PARENT, text,id).addMessages(ms);
        } else if (txt.equals("/stop")) {
            SendMessage m = new SendMessage();
            m.setChatId(id);
            m.setText("IP: " + getIP() + "\nFinalizado");
            try {
                PARENT.sendMessage(m);
            } catch (TelegramApiException ex) {
                ant.error(ex);
                Logger.getLogger(RespuestaConsulta.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                System.out.println("Finalizado por Admin.");
                System.exit(0);
            }
        } else if (txt.equals("/getip")) {
            ms = getIPMessage(ms);
        } else if (txt.startsWith("/allseguimientos")) {
            ms = new GetSeguimientosOP(txt, PARENT.hiloactualizacion).addMessages(ms);
        } else if (txt.startsWith("/send") && id >= 0) {
            ms = new SendTyped(id, text, PARENT).addMessages(ms);
        } else if (txt.equals("/gettimer")) {
            ms = new GetTimer(PARENT).addMessages(ms);
        } else if (txt.startsWith("/settimer")) {
            ms = new SetTimer(PARENT, text).addMessages(ms);
        }
        return ms;
    }

}
