/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estados;

import principal.BotTelegram;
import principal.RespuestaConsulta;
import static comandos.MessageEditor.removeTeclado;
import comandos.usuario.AddAliasToFav;
import java.util.ArrayList;
import org.telegram.telegrambots.api.methods.send.SendMessage;

/**
 *
 * @author eryalus
 */
public class EstadoParadaAñadirNombre extends Estado {

    private final Long id;
    private final String texto;
    private final RespuestaConsulta ant;

    public EstadoParadaAñadirNombre(Long id, String texto, RespuestaConsulta ant, BotTelegram bot) {
        super(bot);
        this.id = id;
        this.texto = texto;
        this.ant = ant;
    }

    @Override
    public ArrayList<SendMessage> response(ArrayList<SendMessage> ms) {
        SendMessage m = new SendMessage();
        switch (texto.toLowerCase()) {
            case "salir":
                m.setText("Ha vuelto al inicio");
                PARENT.putEstado(id, Estado.ESTADO_GENERAL);
                m = removeTeclado(m);
                break;
            case "cancelar":
                m.setText("Escriba el numero de parada a añadir");
                PARENT.putEstado(id, Estado.ESTADO_PARADA_ADD);
                break;
            default:
                AddAliasToFav aatf = new AddAliasToFav(texto, id, PARENT.getUltima_parada_fav(id), PARENT.getConnection(), ant);
                ms = aatf.addMessages(ms);
                if (aatf.isOk()) {
                    PARENT.putEstado(id, Estado.ESTADO_PARADA_ADD);
                }
                return ms;

        }
        ms.add(m);
        return ms;
    }

}
