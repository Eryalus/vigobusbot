/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estados;

import datos.InfoParada;
import principal.BotTelegram;
import static comandos.MessageEditor.removeTeclado;
import comandos.usuario.SeguimientoLinea;
import java.util.ArrayList;
import org.telegram.telegrambots.api.methods.send.SendMessage;

/**
 *
 * @author eryalus
 */
public class EstadoTiempoParada extends Estado {

    private Long id;
    private String texto;

    public EstadoTiempoParada(Long id, String texto, BotTelegram bot) {
        super(bot);
        this.id = id;
        this.texto = texto;
    }

    @Override
    public ArrayList<SendMessage> response(ArrayList<SendMessage> ms) {
        SendMessage m = new SendMessage();
        if (texto.toLowerCase().equals("salir")) {
            m.setText("No se va a realizar ningún seguimiento");
            m = removeTeclado(m);
            PARENT.putEstado(id, Estado.ESTADO_GENERAL);
        } else {
            try {
                Integer minutos = Integer.parseInt(texto);
                if (minutos < 3) {
                    m.setText("No ha introducido un numero valido");
                } else {
                    String linea = PARENT.getLineas_solicitadas(id);
                    InfoParada inf = PARENT.getUltima_parada(id);
                    ms = new SeguimientoLinea(id, linea, minutos, inf, PARENT).addMessages(ms);
                    PARENT.putEstado(id, Estado.ESTADO_GENERAL);
                    return ms;
                }
            } catch (NumberFormatException ex) {
                m.setText("No ha introducido un numero valido");
            }
        }
        ms.add(m);
        return ms;
    }

}
