/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package principal;

import utils.Log;
import acciones.Action;
import acciones.administrador.AccionAdmin;
import acciones.generales.AccionNoAdmin;
import dataBase.Soap;
import datos.Parada;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.methods.updatingmessages.EditMessageReplyMarkup;
import org.telegram.telegrambots.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.api.objects.CallbackQuery;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.exceptions.TelegramApiException;
import utils.Basics;
import static utils.Basics.setInline;

/**
 *
 * @author eryalus
 */
public class RespuestaConsulta extends Thread {

    private Thread hilo = null;
    private final Update UPDATE;
    private final BotTelegram PARENT;
    private String texto_log = "";
    private String error = "";

    public RespuestaConsulta(Update update, BotTelegram parent) {
        this.UPDATE = update;
        this.PARENT = parent;
    }

    public String error(Exception exception) {
        error += exception.getMessage() + "\n";
        StackTraceElement[] sts = exception.getStackTrace();
        for (StackTraceElement st : sts) {
            error += st.toString() + "\n";
        }
        return error;
    }

    @Override
    public void run() {
        //System.out.println("ID inicio: " + PARENT.estados.get(UPDATE.getMessage().getChatId()));
        if (UPDATE.hasMessage()) {
            // System.out.println(UPDATE.getMessage().getChatId());
            try {
                ArrayList<Long> admins = dataBase.Basics.cargarAdmins(PARENT.regcon, true);
                Action action;
                if (admins.contains(UPDATE.getMessage().getChatId())) {
                    action = new AccionAdmin(UPDATE, PARENT, this);
                } else {
                    action = new AccionNoAdmin(UPDATE, PARENT, this);
                }
                boolean result = action.action();
                if (action instanceof AccionAdmin && !result) { //si es admin pero no es accion de admin
                    action = new AccionNoAdmin(UPDATE, PARENT, this);
                    result = action.action();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                error(ex);
                Log.Log("Error interno:\n" + error);
                SendMessage m = new SendMessage();
                m.setText("Ha ocurrido un error. Inténtelo de nuevo más tarde");
                m.setChatId(UPDATE.getMessage().getChatId());
                try {
                    PARENT.sendMessage(m);
                } catch (Exception exx) {
                    Log.Log("Error interno:\n" + exx.getMessage());
                    exx.printStackTrace();
                }
            }
        } else if (UPDATE.hasCallbackQuery()) {
            CallbackQuery query = UPDATE.getCallbackQuery();
            String cmd = query.getData();
            Integer msg = query.getMessage().getMessageId();
            Long chat = query.getMessage().getChatId();
            EditMessageText edit = new EditMessageText();
            edit.setChatId(chat);
            edit.setMessageId(msg);
            if (cmd.startsWith("reload-")) {
                String[] parts = cmd.split("-");
                if (parts.length == 2) {
                    String parada = parts[1];
                    try {
                        Integer par = Integer.parseInt(parada);
                        Parada p = dataBase.Basics.cargarParadas(PARENT.getConnection(), false).get(par);
                        ArrayList<SendMessage> ms = Basics.getDataInMessages(Soap.getDatos(par), new ArrayList<>(), par, p);
                        edit.setText(ms.get(0).getText());
                        edit.setParseMode("html");
                        PARENT.editMessageText(edit);
                        EditMessageReplyMarkup msgedit = new EditMessageReplyMarkup();
                        msgedit.setChatId(chat);
                        msgedit.setMessageId(msg);
                        msgedit.setReplyMarkup(setInline("reload-" + par, "Recargar"));
                        PARENT.editMessageReplyMarkup(msgedit);
                    } catch (NumberFormatException ex) {

                    } catch (IOException ex) {
                        Logger.getLogger(RespuestaConsulta.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(RespuestaConsulta.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (TelegramApiException ex) {
                        Logger.getLogger(RespuestaConsulta.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
        // System.out.println("ID inicio: " + PARENT.estados.get(UPDATE.getMessage().getChatId()));
    }

    @Override
    public void start() {
        if (hilo == null) {
            hilo = new Thread(this, "Hilo de respuesta");
            hilo.start();
        }
    }
}
