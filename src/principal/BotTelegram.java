/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package principal;

import acciones.AutoDump;
import comprobaciones.HiloActualizacionParadas;
import dataBase.ReguladorConexion;
import datos.InfoParada;
import datos.Parada;
import datos.ParadaFavorita;
import datos.Persona;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;

/**
 *
 * @author ad_ri
 */
public class BotTelegram extends TelegramLongPollingBot {

    public HiloActualizacionParadas hiloactualizacion;
    //protected HashMap<Long, InfoParada> ultima_parada = new HashMap<>();
    protected AutoDump autodump;

    public AutoDump getAutodump() {
        return autodump;
    }

    public AutoDump setAutodump(AutoDump autodump) {
        this.autodump = autodump;
        return autodump;
    }

    public boolean putLineas_solicitadas(Long key, String value) {
        return dataBase.Basics.updateLineaSolicitada(key, value, getConnection());
    }

    public boolean putUltima_parada_fav(Long key, ParadaFavorita value) {
        return dataBase.Basics.updateUltFav(key, value.getFavoritaI(), getConnection());
    }

    public boolean putUltima_parada(Long key, InfoParada value) {
        return dataBase.Basics.updateUltimaParada(key, value.toString(), getConnection());
    }

    /**
     * true para hacer log de todo y false para hacer log solo de los errores
     */
    private boolean flag_log = false;

    public boolean isFullLog() {
        return flag_log;
    }

    public boolean setFlagLog(boolean flag) {
        boolean temp = flag_log;
        flag_log = flag;
        return temp;
    }
    /**
     * Para el "/send" del admin, el String contiene a que gente enviar.
     */
    protected HashMap<Long, String> toSend = new HashMap<>();

    public InfoParada getUltima_parada(Long id_chat) {
        return InfoParada.reverseToString(dataBase.Basics.getUltimaParada(id_chat, getConnection()));
    }

    public ParadaFavorita getUltima_parada_fav(Long id_chat) {
        Integer ultFav = dataBase.Basics.getUltFav(id_chat, getConnection());
        try {
            ArrayList<ParadaFavorita> paradasFavoritas = dataBase.Basics.getParadasFavoritas(id_chat, getConnection().createStatement());
            for (ParadaFavorita f : paradasFavoritas) {
                if (Objects.equals(f.getFavoritaI(), ultFav)) {
                    return f;
                }
            }
        } catch (SQLException ex) {
        }
        return null;
    }

    public HiloActualizacionParadas getHiloactualizacion() {
        return hiloactualizacion;
    }

    public String getLineas_solicitadas(Long id_chat) {
        return dataBase.Basics.getLineaSolicitada(id_chat, getConnection());
    }

    public ReguladorConexion getRegcon() {
        return regcon;
    }

    /**
     * Relaciona el ID del chat con un numero que identifica el estado en el que
     * se encuentra la conversacion. 0 para el nivel general 1 para introductor
     * tiempo parada 2 introducir numero parada a eliminar 3 introducir numero
     * de parada a añadir. 4 Pregunta si poner alias a favorita. 5 pone alias
     * parada favorita. 6 texto a enviar el admin. 7 envia geo para paradas
     * cercanas. 8 ara buscar parada por texto.
     * @param id_chat
     * @return 
     */
    public Integer getEstados(Long id_chat) {
        return dataBase.Basics.getEstado(id_chat, getConnection());
    }

    public boolean putEstado(Long key, Integer value) {
        return dataBase.Basics.updateEstado(key, value, getConnection());
    }
    protected ReguladorConexion regcon = null;

    public String toSend(Long id) {
        return toSend.get(id);
    }

    public Connection getConnection() {
        return regcon.getConnection();
    }

    public String putToSend(Long id, String txt) {
        return toSend.put(id, txt);
    }

    public boolean añadirParadasURL(String url) {
        ArrayList<Parada> al = utils.ConexionPaginas.cargarParadasFromVitrasa(url);
        if (al.isEmpty()) {
            return false;
        }
        System.out.println("Se han encontrado " + al.size() + " paradas");
        int[] d = dataBase.Basics.actualizarParadas(al, regcon.getConnection());
        System.out.println("Se han actulizado " + d[0] + " paradas");
        System.out.println("Se han añadido " + d[1] + " paradas");
        return true;
    }

    public void añadirParadasHTML(String ruta) {
        ArrayList<Parada> al = utils.ConexionPaginas.cargarParadasHTML(ruta);
        System.out.println("Se han encontrado " + al.size() + " paradas");
        int[] d = dataBase.Basics.actualizarParadas(al, regcon.getConnection());
        System.out.println("Se han actulizado " + d[0] + " paradas");
        System.out.println("Se han añadido " + d[1] + " paradas");
    }

    public BotTelegram(HashMap<Long, Persona> map, HashMap<Integer, Parada> paradas, ReguladorConexion regcon) {
        this.regcon = regcon;
        long tiempo = 15 * 1000;
        this.hiloactualizacion = new HiloActualizacionParadas(tiempo, this);
    }

    public void empezarHiloActualizacion() {
        this.hiloactualizacion.start();
    }

    /**
     *
     * @return
     */
    @Override
    public String getBotToken() {
        return "428562911:AAEgdKRpqXaOM8_Pk6dwH0XQDhHVEXTtspc"; //otro
       //  return "393941482:AAEUwN-jQfYP8UKqm9_K2KlzduQmI4kA_hs";
    }

    @Override
    public void onUpdateReceived(Update update) {
        RespuestaConsulta hilo = new RespuestaConsulta(update, this);
        hilo.start();
    }

    @Override
    public String getBotUsername() {
        return "busvigobot"; //otro
       // return "pruebas_java_bot";
    }

}
